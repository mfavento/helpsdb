﻿using System;
using System.Collections.Generic;

namespace testDB
{
	public class MenuListData: List<MenuItem>

	{
		public MenuListData ()
		{
			this.Add(new MenuItem() {
				Title = "ACCOUNT",
				IconSource = "ic_user.png",
				TargetType = typeof(MissingPage)
			});

			this.Add(new MenuItem() {
				Title = "INVESTMENTS",
				IconSource = "ic_performance.png",
				TargetType = typeof(MissingPage)
			});

			this.Add(new MenuItem() {
				Title = "TECHNICAL",
				IconSource = "ic_settings.png",
				TargetType = typeof(MissingPage)
			});

			this.Add(new MenuItem() {
				Title = "EDUCATION",
				IconSource = "ic_hat.png",
				TargetType = typeof(MissingPage)
			});

			this.Add(new MenuItem() {
				Title = "ADVICE",
				IconSource = "ic_info.png",
				TargetType = typeof(MissingPage)
			});
					
	
		}
	}
}

