﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Json;
using Newtonsoft.Json;
using ModernHttpClient;

namespace testDB.Droid
{
	[Activity (Label = "Register")]

	public class RegisterStudent2 : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.register_student);

			//finding fields...
			Button btnRegister = FindViewById<Button> (Resource.Id.btn_register);

			TextView etStudentID = FindViewById<TextView> (Resource.Id.et_studentID);
			TextView etDOB = FindViewById<TextView> (Resource.Id.et_dob);

			RadioGroup rgroupGender = FindViewById<RadioGroup>(Resource.Id.rbg_gender);
			RadioButton rbtnGender = FindViewById<RadioButton>(rgroupGender.CheckedRadioButtonId);
			RadioGroup rgroupDegree = FindViewById<RadioGroup>(Resource.Id.rbg_degree);
			RadioButton rbtnDegree = FindViewById<RadioButton>(rgroupDegree.CheckedRadioButtonId);
			RadioGroup rgroupStatus = FindViewById<RadioGroup>(Resource.Id.rbg_status );
			RadioButton rbtnStatus  = FindViewById<RadioButton>(rgroupStatus.CheckedRadioButtonId);

			Spinner spnLanguage = FindViewById<Spinner> (Resource.Id.spin_language);
			Spinner spnOrigin = FindViewById<Spinner> (Resource.Id.spin_origin);
			Spinner spnYear = FindViewById<Spinner> (Resource.Id.spin_year);

			//making adapters
			var spnYearAdapter = ArrayAdapter.CreateFromResource (this, Resource.Array.year_array, Android.Resource.Layout.SimpleSpinnerItem);
			var spnLanAdapter = ArrayAdapter.CreateFromResource (this, Resource.Array.languages_array, Android.Resource.Layout.SimpleSpinnerItem);
			var spnOriAdapter = ArrayAdapter.CreateFromResource (this, Resource.Array.origin_array, Android.Resource.Layout.SimpleSpinnerItem);

			//assigning adapters
			spnLanguage.Adapter = spnLanAdapter;
			spnOrigin.Adapter = spnOriAdapter;
			spnYear.Adapter = spnYearAdapter;

			var spnLanValue = spnLanguage.SelectedItemPosition;
			var spnOriValue = spnOrigin.SelectedItemPosition;
			var spnYearValue = spnYear.SelectedItemPosition;


			//tnRegister.Click += async (sender, e) => {
			btnRegister.Click += async (sender, e) => {
				
		

					if(allFieldsValid()){
						var studentDetails = new Student();
						var spnLanText = spnLanguage.GetItemAtPosition(spnLanValue).ToString();
						var spnOriText = spnOrigin.GetItemAtPosition(spnOriValue).ToString();
						var spnYearText = spnYear.GetItemAtPosition(spnYearValue).ToString();
						//assign studentDetails object values
					studentDetails.StudentId = etStudentID.Text;
					studentDetails.DateOfBirth = etDOB.Text;
					studentDetails.Degree = rbtnDegree.Text;
					studentDetails.Status = rbtnStatus.Text;
					studentDetails.Gender = rbtnGender.Text;
					studentDetails.FirstLanguage = spnLanText;
					studentDetails.CountryOrigin = spnOriText;
					studentDetails.DegreeDetails = spnYearText;
					studentDetails.CreatorId = "123456";
					string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject (studentDetails);
					Console.WriteLine(jsonString);
					ServerResponse response = await FetchHelps(jsonString);
					ParseAndDisplay(response);
				}

						

						//var json = await FetchRequestAsync (url);
						// ParseAndDisplay (json);

			
			};

		}

		private void ParseAndDisplay (ServerResponse response){

			//Toast.MakeText (this, "Response value = " + response.IsSuccess, ToastLength.Short).Show(); 
			Toast.MakeText (this, "Response value = ", ToastLength.Short).Show(); 
			Console.WriteLine ("Butts");



		}

		private async Task<ServerResponse> FetchHelps (string jsonString)
		{
			string url = "http://helpsdb.cloudapp.net/api/student/register";

			var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
			httpWebRequest.ContentType = "application/json";
			httpWebRequest.Method = "POST";
			httpWebRequest.Headers["AppKey"] = "999998";
			httpWebRequest.ContentLength = jsonString.Length;
			httpWebRequest.ReadWriteTimeout = 4000;
	
			byte[] b1 = System.Text.Encoding.UTF8.GetBytes (jsonString);
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
			ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

			var response = (WebResponse) httpWebRequest.GetResponse();
			var stream = response.GetResponseStream ();
			stream.Write (b1, 0, jsonString.Length);
			var reader = new StreamReader (stream);
			string responseText = reader.ReadToEnd ();

			Console.WriteLine(responseText);

			ServerResponse newServerResponse = JsonConvert.DeserializeObject<ServerResponse>(responseText);


			return newServerResponse;

			}






		private bool allFieldsValid(){
			return true;
		}

	}

}
