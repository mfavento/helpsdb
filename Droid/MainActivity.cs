﻿using System;
using Android.Views.InputMethods;
using Android.Support.V4.View;
using Android.Support.V4.Widget;

using Android.Support.V7.App;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Preferences;
using System.Net;
using System.Json;
using Newtonsoft.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Collections.Specialized;
using System.Collections;
using System.Globalization;
using Android.Support.V7.Widget;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;


/*
 * 
 * This class is what the user sees when they attempt to log in.
 * 
 * TODO: 
 * If internet is down display error when logging in after certain amount of time
 * Error handling in general
 * UI improvement
 * 
 */
using Android.Content.PM;

namespace testDB.Droid
{
	[Activity (Label = "Login", MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : AppCompatActivity
	{

		private SupportToolbar mToolbar;

		private string password;
		private EditText studentEt;
		private EditText passEt;
		private ProgressDialog progress;
		private ISharedPreferences prefs;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			//view this file in layouts
			SetContentView (Resource.Layout.Main);
			mToolbar = FindViewById<SupportToolbar> (Resource.Id.toolbar);
			SetSupportActionBar (mToolbar);
			string now = DateTime.Now.ToLocalTime ().ToString ();
			password = GetString (Resource.String.password);

			Toast.MakeText (this, now, ToastLength.Short).Show ();

			//finding UI elements in the above layout...
			Button button = FindViewById<Button> (Resource.Id.btnLogin);
			passEt = FindViewById<EditText> (Resource.Id.et_passwordID);
			studentEt = FindViewById<EditText> (Resource.Id.et_studentID);

			progress = getProgess ("Logging in...");

			// When the user clicks the button ...
			button.Click += delegate {
				if (allFieldsValid ())
					getStudent ();
			};
		}



		private bool allFieldsValid ()
		{
			if (!System.Text.RegularExpressions.Regex.IsMatch (studentEt.Text, "^[0-9]{8}$")) {
				Toast.MakeText (this, "Invalid student ID or password", ToastLength.Short).Show ();

				return false;
			} 

			if (!(passEt.Text == password)) {
				Toast.MakeText (this, "Invalid student ID or password", ToastLength.Short).Show ();
				return false;
			}

			return true;
		}

		/*
		 * Gets student from database, for more indepth review of 'WebClient' please see Trello.
		 * 
		 */

		private void getStudent ()
		{
			
			Uri uri = new Uri (GetString (Resource.String.service_domain) + "student/" + studentEt.Text);
			var client = new MyWebClient ();
			client.Headers ["AppKey"] = "999998";
			//WebClientOperations is a custom class that displays errors

			if (WebClientOperations.TestConnectionDB (client)) {
				progress.Show ();
				client.DownloadDataAsync (uri);
			} else {
				WebClientOperations.WebClientError ("No result from server!", client, Application.Context);
				progress.Dismiss ();

			}
		
			client.DownloadDataCompleted += (s, e) => {

				//if there is a result (student was found)...

				if (e.Result != null) {
					var studentIdForPrefs = studentEt.Text;
					var text = System.Text.Encoding.UTF8.GetString (e.Result);
					AccountOperations.setPrefsId (studentIdForPrefs, this);


					prefs = AccountOperations.getSharedPreferences ();
					ISharedPreferencesEditor editor = prefs.Edit ();

					JObject o = JObject.Parse (text);

					if ((string)o.SelectToken ("Result.studentID") != null) {

						//already registered on the helps system

						String studentNo = (string)o.SelectToken ("Result.studentID");
						Console.WriteLine (studentNo);
						editor.PutString ("StudentIdNumber", studentNo);              
						editor.Commit ();
						progress.Dismiss ();
						StartActivity (typeof(AccountOverview));
						
					} else {

						/* 		
			 * This method must be edited to ensure the data being returned is valid (exists in the UTS database)
			 * For now, it will assume that any input that matches the regex is an actual student,
			 * only they are using the program for the first time
			*/ 

						Toast.MakeText (this, "StudentID not found", ToastLength.Short).Show ();
						editor.PutString ("StudentIdNumber", studentEt.Text);              
						editor.Commit ();
						StartActivity (typeof(PrefilledDataActivity));
					}
				} else {

					WebClientOperations.WebClientError ("No result from server!", client, Application.Context);
				}
			};

		}


		/*
		 * This method returns a progress dialog with all the needed fields completed
		 */

		private ProgressDialog getProgess (String message)
		{
			progress = new ProgressDialog (this);
			progress.Indeterminate = true;
			progress.SetProgressStyle (ProgressDialogStyle.Spinner);
			progress.SetMessage (message);
			progress.SetCancelable (false);
			return progress;
		}




	}
}
	
