﻿using Android.Support.V4.Widget;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;

namespace testDB.Droid
{
	[Activity (Label = "Booking Confirmed", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class StaticBookingConfirmed : Activity
	{
		private DrawerLayout mDrawer;
		private MyActionBarDrawerToggle mDrawerToggle;
		private ListView mDrawerList;

		private string mDrawerTitle;
		private string mNavTitle;
		private string[] mMenuTitles;


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.static_booking_confirmed);



			Button btnLogin = FindViewById<Button> (Resource.Id.btn_overview);
			btnLogin.Click += delegate {
				StartActivity (typeof(AccountOverview));
			};
		}

		public override void OnBackPressed ()
		{
			// This prevents a user from being able to hit the back button and leave the login page.
		}


		private void SelectItem (int position)
		{

			mDrawerList.SetItemChecked (position, true);

			switch (position) {

			//home
			case 0:
				StartActivity (typeof(AccountOverview));
				break;
			//settings
			case 1: 
				StartActivity (typeof(SettingsActivity));

				break;
			//make booking
			case 2:
				StartActivity (typeof(WorkshopChooser));

				break;
			//FAQ
			case 3:
				Toast.MakeText (this, "FAQ", ToastLength.Short).Show ();
				var uri = Android.Net.Uri.Parse ("https://helps-booking.uts.edu.au/index.cfm?scope=help");
				var intent = new Intent (Intent.ActionView, uri);
				StartActivity (intent);
				break;

			//Programs
			case 4:
				StartActivity (typeof(SessionChooserPrograms));
				break;

			}
			mDrawer.CloseDrawers ();
			mDrawerToggle.SyncState ();		
		}


		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			// Pass the event to ActionBarDrawerToggle, if it returns
			// true, then it has handled the app icon touch event
			if (mDrawerToggle.OnOptionsItemSelected (item)) {
				return true;
			}
			// Handle your other action bar items...

			return false;
		}


	}
}

