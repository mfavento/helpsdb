﻿using System;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.Widget;
using System.Collections.Generic;
using Newtonsoft.Json.Converters;
using Android.Support.V4.View;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;

using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Json;
using System.Collections.Specialized;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Android.Webkit;
using Android.Content.PM;

namespace testDB.Droid
{
	[Activity (Label = "Workshop Chooser", ScreenOrientation = ScreenOrientation.Portrait)]

	public class WorkshopChooser : AppCompatActivity
	{
		//navigation variables
		private DrawerLayout mDrawer;
		private MyActionBarDrawerToggle mDrawerToggle;
		private ListView mDrawerList;
		private SupportToolbar mToolbar;
		private ArrayAdapter mAdapter;
		private bool fragLoadedFlag = false;
		private bool notDefaultFlag = false;

		private MyWebClient client;

		List<String> workshopSetNames = new List<String> ();
		List<WorkshopSet> workshopSets = new List<WorkshopSet> ();
		Spinner spnWorkshopType;
		ChooserFragment chooserFragment;
		ProgressDialog progress;
		ProgressDialog progress2;

		//turn into const
		const String WORKSHOP_LABEL = "Select Workshop Type";

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.session_chooser);
			spnWorkshopType = FindViewById<Spinner> (Resource.Id.spn_workshops);

			mToolbar = FindViewById<SupportToolbar> (Resource.Id.toolbar);
			SetSupportActionBar (mToolbar);
			setupNavDrawer ();

			//instantiate fragment
			//progress dialog start


			if (workshopSets.Count < 1) {
				GetWorkshopSets ();
			}
			//fakeValues ();
			setSpinner ();

		}



		private void spinner_ItemSelected (object sender, AdapterView.ItemSelectedEventArgs e)
		{
			
			Spinner spinner = (Spinner)sender;
			var selectedWorkshopName = spinner.GetItemAtPosition (e.Position);
			//is not the label
			if (e.Position != 0) {
				notDefaultFlag = true;
				fragLoadedFlag = false;
				client.CancelAsync ();

				//insert fragment into container through async
				for (int i = 0; i < workshopSets.Count; i++) {
					if (selectedWorkshopName.Equals (workshopSets [i].Name)) {
						createFragment (workshopSets [i]);

					}

				}



			}

		}

		public void setFragLoadedTrue ()
		{
			fragLoadedFlag = true;

		}

		private void refreshAdapter ()
		{
			chooserFragment.refreshAdapter ();

		}

		private void GetWorkshopSets ()
		{
			
			progress = ProgressDialog.Show (this, "Please wait...", "Searching for Workshop sets...", true);


			workshopSetNames.Add (WORKSHOP_LABEL);
			Uri uri = new Uri ("http://helpsdb.cloudapp.net/api/workshop/workshopSets/true");
			client = new MyWebClient ();
			client.Proxy = null;
			client.Headers ["AppKey"] = "999998";


			client.DownloadDataAsync (uri);
			client.DownloadProgressChanged += (sender, e) => {
				Console.WriteLine ("Bytes: " + e.BytesReceived + " of " + e.TotalBytesToReceive);


			};


			client.DownloadDataCompleted += (s, e) => {

				var text = System.Text.Encoding.UTF8.GetString (e.Result);


				var json = (JObject)JsonConvert.DeserializeObject (text);


				var array = (JArray)json ["Results"];
				var list = array.Select (xyz => new WorkshopSet ((int)xyz ["id"], (string)xyz ["name"]));



				workshopSets = list.ToList ();


				foreach (WorkshopSet wrk in list) {
					workshopSetNames.Add (wrk.Name);

				}


				if (workshopSets.Count > 1) {
					setSpinner ();
				}
				progress.Dismiss ();
					
			};
				
		}


		public void createFragment (WorkshopSet workshopSet)
		{
			progress2 = ProgressDialog.Show (this, "Please wait...", "Searching for workshops...", true);


			chooserFragment = new ChooserFragment ();
			var ft = FragmentManager.BeginTransaction ();
			chooserFragment.addListing (workshopSet);
			ft.Replace (Resource.Id.fragment_container, chooserFragment);
			ft.Commit ();
			progress2.Dismiss ();
		}


		public void setSpinner ()
		{
			ArrayAdapter adapter = new ArrayAdapter (this, Android.Resource.Layout.SimpleSpinnerItem, workshopSetNames);
			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spnWorkshopType.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (spinner_ItemSelected);
			spnWorkshopType.Adapter = adapter;
		}



		public void viewWorkshopFragment (Workshop workshop)
		{
			Intent intent;
			if (workshop.getPlaces () && workshop != null) {
				intent = new Intent (this, typeof(WorkshopActivity));
			} else {
				intent = new Intent (this, typeof(WorkshopWaitingActivity));

			}
			string jsonModel = JsonConvert.SerializeObject (workshop, new IsoDateTimeConverter ());
			intent.PutExtra ("time", workshop.Time);
			intent.PutExtra ("startDate", workshop.StartDate);
			intent.PutExtra ("endDate", workshop.EndDate);
			intent.PutExtra ("model", jsonModel);
			StartActivity (intent);

		
		}

		private void setupNavDrawer ()
		{
			mDrawer = FindViewById<DrawerLayout> (Resource.Id.drawer_layout);
			mDrawerList = FindViewById<ListView> (Resource.Id.left_drawer);
			mDrawerList.Tag = 0;
			String[] navTitles = Resources.GetStringArray (Resource.Array.nav_array);

			mAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, navTitles);
			mDrawerList.Adapter = mAdapter;
			mDrawerList.ItemClick += (sender, args) => SelectItem (args.Position);
			int intTitle;
			Int32.TryParse (Title, out intTitle);

			mDrawerToggle = new MyActionBarDrawerToggle (
				this,
				mDrawer,
				intTitle,
				intTitle);

			mDrawer.SetDrawerListener (mDrawerToggle);
			SupportActionBar.SetHomeButtonEnabled (true);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);
			SupportActionBar.SetDisplayShowTitleEnabled (true);
			mDrawerToggle.SyncState ();
		}

		private void SelectItem (int position)
		{

			mDrawerList.SetItemChecked (position, true);

			switch (position) {

			//home
			case 0:
				StartActivity (typeof(AccountOverview));
				break;
			//settings
			case 1: 
				StartActivity (typeof(SettingsActivity));

				break;
			//make booking
			case 2:
				StartActivity (typeof(WorkshopChooser));

				break;
			//FAQ
			case 3:
				Toast.MakeText (this, "FAQ", ToastLength.Short).Show ();
				var uri = Android.Net.Uri.Parse ("https://helps-booking.uts.edu.au/index.cfm?scope=help");
				var intent = new Intent (Intent.ActionView, uri);
				StartActivity (intent);
				break;

			//Programs
			case 4:
				StartActivity (typeof(SessionChooserPrograms));
				break;

			}
			mDrawer.CloseDrawers ();
			mDrawerToggle.SyncState ();		
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {

			case Android.Resource.Id.Home:
				//The hamburger icon was clicked which means the drawer toggle will handle the event
				mDrawerToggle.OnOptionsItemSelected (item);

				return true;
			
			case Resource.Id.action_sort_date:
				if (notDefaultFlag && fragLoadedFlag) {
					
					sortWorkshopsBy (Constants.SORT_DATE);
				}
				return true;

			case Resource.Id.action_sort_location:
				if (notDefaultFlag && fragLoadedFlag) {
					
					sortWorkshopsBy (Constants.SORT_LOCATION);
				}
				return true;

			case Resource.Id.action_sort_topic:
				if (notDefaultFlag && fragLoadedFlag) {
					
					sortWorkshopsBy (Constants.SORT_TOPIC);
				}
				return true;

			default:
				return base.OnOptionsItemSelected (item);
			}
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.action_menu_session, menu);
			return base.OnCreateOptionsMenu (menu);
		}

		protected override void OnPostCreate (Bundle savedInstanceState)
		{
			base.OnPostCreate (savedInstanceState);
			mDrawerToggle.SyncState ();
		}






		private void sortWorkshopsBy (int sortChoice)
		{
			if (fragLoadedFlag)
				chooserFragment.sortWorkshops (sortChoice);
				
		}
	}
}

