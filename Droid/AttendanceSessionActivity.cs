﻿using Android.Support.V4.Widget;
using System;
using System.Collections.Generic;
using Newtonsoft.Json.Converters;
using Android.Support.V4.View;

using System.Linq;
using System.Text;
using Android.Preferences;
using Android.Support.V7.Widget;
using Android.Support.V7.App;

using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Text;
using Java.Lang;
using Android.Content.PM;

namespace testDB.Droid
{
	[Activity (Label = "Attendance", ScreenOrientation = ScreenOrientation.Portrait)]

	public class AttendanceSessionActivity : AppCompatActivity
	{
		private DrawerLayout mDrawer;
		private MyActionBarDrawerToggle mDrawerToggle;
		private ListView mDrawerList;
		private List<NavDrawerListItem> navigationDrawerItems;
		private ArrayAdapter mAdapter;
		ISharedPreferencesEditor editor;
		private Context context;
		private SessionSet mSessionSet;
		string studentIdPrefs;
		ISharedPreferences prefs;
		private SupportToolbar mToolbar;
		Session mSession;
		private string mNotes;
		private EditText etNotes;




		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.attendance_activity);
			context = Application.Context;

			string json = Intent.GetStringExtra ("model");

			mSession = JsonConvert.DeserializeObject<Session> (json,
				new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy HH:ss" });
			
			mToolbar = FindViewById<SupportToolbar> (Resource.Id.toolbar);
			SetSupportActionBar (mToolbar);

			getPrefsId ();
			setupNavDrawer ();

			//finding fields...
			etNotes = FindViewById<EditText> (Resource.Id.et_notes);


			TextView txtAttend = FindViewById<TextView> (Resource.Id.txt_attend);

			txtAttend.Text = "\nDid you attend\n " + mSession.SessionType + "?";

			fillFields ();

			Button btnSubmit = FindViewById<Button> (Resource.Id.btn_submit);
			Button btnNotAttend = FindViewById<Button> (Resource.Id.btn_not_attend);


			btnSubmit.Click += delegate {

				//check if min 30 chars
				if (checkBooking (etNotes.Text)) {
					updateBooking ();
				} else {
					Toast.MakeText (this, "Please enter a minimum of 30 characters", ToastLength.Short).Show ();
				}
			};

			btnNotAttend.Click += delegate {
				//are u sure

				UpdateHelper.cancelSession (this, mSession, studentIdPrefs);
				Thread.Sleep (1000);
				StartActivity (typeof(StaticBookingNotAttended));
			};
		}


		private bool checkBooking (string notesEntry)
		{
			
			if (TextUtils.IsEmpty (notesEntry) || notesEntry.Length < 30)
				return false;
			else
				return true;

		}


		private void updateBooking ()
		{
			mNotes = etNotes.Text;
			UpdateHelper.attendSession (this, mSession, mNotes, studentIdPrefs);

			StartActivity (typeof(StaticConfirmedAttendance));
		}


		public void setBookingSet (SessionSet sessionSet)
		{
			mSessionSet = sessionSet;

		}


		private void fillFields ()
		{
			
			
		}



		private void setupNavDrawer ()
		{
			mDrawer = FindViewById<DrawerLayout> (Resource.Id.drawer_layout);
			mDrawerList = FindViewById<ListView> (Resource.Id.left_drawer);
			mDrawerList.Tag = 0;
			string[] navTitles = Resources.GetStringArray (Resource.Array.nav_array);

			mAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, navTitles);
			mDrawerList.Adapter = mAdapter;
			mDrawerList.ItemClick += (sender, args) => SelectItem (args.Position);
			int intTitle;
			Int32.TryParse (Title, out intTitle);

			mDrawerToggle = new MyActionBarDrawerToggle (
				this,
				mDrawer,
				intTitle,
				intTitle);

			mDrawer.SetDrawerListener (mDrawerToggle);
			SupportActionBar.SetHomeButtonEnabled (true);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);
			SupportActionBar.SetDisplayShowTitleEnabled (true);
			mDrawerToggle.SyncState ();
		}

		private void SelectItem (int position)
		{

			mDrawerList.SetItemChecked (position, true);

			switch (position) {

			//home
			case 0:
				StartActivity (typeof(AccountOverview));
				break;
			//settings
			case 1: 
				StartActivity (typeof(SettingsActivity));
				break;
			//make booking
			case 2:
				StartActivity (typeof(WorkshopChooser));
				break;
			//FAQ
			case 3:
				var uri = Android.Net.Uri.Parse ("https://helps-booking.uts.edu.au/index.cfm?scope=help");
				var intent = new Intent (Intent.ActionView, uri);
				StartActivity (intent);
				break;
			//Programs
			case 4:
				
				StartActivity (typeof(SessionChooserPrograms));				
				break;
			}
			mDrawer.CloseDrawers ();
			mDrawerToggle.SyncState ();		
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {

			case Android.Resource.Id.Home:
				mDrawerToggle.OnOptionsItemSelected (item);
				return true;

			case Resource.Id.action_settings:
				//Refresh
				StartActivity (typeof(SettingsActivity));
				return true;


			default:
				return base.OnOptionsItemSelected (item);
			}
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.action_menu_settings, menu);
			return base.OnCreateOptionsMenu (menu);
		}

		protected override void OnPostCreate (Bundle savedInstanceState)
		{
			base.OnPostCreate (savedInstanceState);
			mDrawerToggle.SyncState ();
		}


		private void getPrefsId ()
		{
			prefs = AccountOperations.getSharedPreferences ();
			studentIdPrefs = prefs.GetString ("StudentIdNumber", string.Empty);
			editor = prefs.Edit ();

		}

	}

}
