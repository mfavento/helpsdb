﻿using Android.Support.V4.Widget;
using System;
using System.Collections.Generic;
using Newtonsoft.Json.Converters;
using Android.Support.V4.View;

using System.Linq;
using System.Text;
using Android.Preferences;
using Android.Support.V7.Widget;
using Android.Support.V7.App;

using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Content.PM;

namespace testDB.Droid
{
	[Activity (Label = "Settings", ScreenOrientation = ScreenOrientation.Portrait)]

	public class SettingsActivity : AppCompatActivity
	{
		private DrawerLayout mDrawer;
		private MyActionBarDrawerToggle mDrawerToggle;
		private ListView mDrawerList;
		private ArrayAdapter mAdapter;
		ISharedPreferencesEditor editor;
		private Context context;
		private BookingSet mBookingSet;

		String studentIdPrefs;
		ISharedPreferences prefs;
		private SupportToolbar mToolbar;

		private bool tenMins;
		private bool thirtyMins;
		private bool oneDay;
		private bool oneWeek;

		CheckBox chkTen;
		CheckBox chkThirty;
		CheckBox chkDay;
		CheckBox chkWeek;



		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.settings_activity);
			context = Application.Context;

			mToolbar = FindViewById<SupportToolbar> (Resource.Id.toolbar);
			SetSupportActionBar (mToolbar);

			getPrefsId ();
	
			setupNavDrawer ();

			//finding fields...


			chkTen = FindViewById<CheckBox> (Resource.Id.checkbox_ten_mins);
			chkThirty = FindViewById<CheckBox> (Resource.Id.checkbox_thirty_mins);
			chkDay = FindViewById<CheckBox> (Resource.Id.checkbox_one_day);
			chkWeek = FindViewById<CheckBox> (Resource.Id.checkbox_one_week);
			fillFields ();

		




			Button btnLogout = FindViewById<Button> (Resource.Id.btn_log_out);
			Button btnPrivacy = FindViewById<Button> (Resource.Id.btn_privacy_policy);
			Button btnTerms = FindViewById<Button> (Resource.Id.btn_terms_service);


			btnLogout.Click += delegate {

				StartActivity (typeof(MainActivity));

			};

			btnPrivacy.Click += delegate {

				var uri = Android.Net.Uri.Parse (GetString (Resource.String.privacy_policy_link));
				var intent = new Intent (Intent.ActionView, uri);
				StartActivity (intent);
			};

			btnTerms.Click += delegate {

				var uri = Android.Net.Uri.Parse (GetString (Resource.String.terms_of_service_link));
				var intent = new Intent (Intent.ActionView, uri);
				StartActivity (intent);
			};





		}



		public void setBookingSet (BookingSet bookingSet)
		{
			mBookingSet = bookingSet;

		}


		private void fillFields ()
		{

			if (tenMins)
				chkTen.Checked = true;
			if (thirtyMins)
				chkThirty.Checked = true;
			if (oneDay)
				chkDay.Checked = true;
			if (oneWeek)
				chkWeek.Checked = true;


			chkTen.CheckedChange += (sender, e) => {
				editor.PutBoolean ("ReminderTenMins", e.IsChecked);
				editor.Commit ();
			};


			chkThirty.CheckedChange += (sender, e) => {
				editor.PutBoolean ("ReminderThirtyMins", e.IsChecked);
				editor.Commit ();
			};


			chkDay.CheckedChange += (sender, e) => {
				editor.PutBoolean ("ReminderOneDay", e.IsChecked);
				editor.Commit ();
			};


			chkWeek.CheckedChange += (sender, e) => {
				editor.PutBoolean ("ReminderOneWeek", e.IsChecked);
				editor.Commit ();

			};
			
		}

		private void refreshBookings ()
		{


		}

		private void setupNavDrawer ()
		{
			mDrawer = FindViewById<DrawerLayout> (Resource.Id.drawer_layout);
			mDrawerList = FindViewById<ListView> (Resource.Id.left_drawer);
			mDrawerList.Tag = 0;
			String[] navTitles = Resources.GetStringArray (Resource.Array.nav_array);

			mAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, navTitles);
			mDrawerList.Adapter = mAdapter;
			mDrawerList.ItemClick += (sender, args) => SelectItem (args.Position);
			int intTitle;
			Int32.TryParse (Title, out intTitle);

			mDrawerToggle = new MyActionBarDrawerToggle (
				this,
				mDrawer,
				intTitle,
				intTitle);

			mDrawer.SetDrawerListener (mDrawerToggle);
			SupportActionBar.SetHomeButtonEnabled (true);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);
			SupportActionBar.SetDisplayShowTitleEnabled (true);
			mDrawerToggle.SyncState ();
		}

		private void SelectItem (int position)
		{

			mDrawerList.SetItemChecked (position, true);

			switch (position) {

			//home
			case 0:
				StartActivity (typeof(AccountOverview));
				break;
			//settings
			case 1: 
				break;
			//make booking
			case 2:
				StartActivity (typeof(WorkshopChooser));
				break;
			//FAQ
			case 3:
				var uri = Android.Net.Uri.Parse (GetString (Resource.String.faq_link));
				var intent = new Intent (Intent.ActionView, uri);
				StartActivity (intent);
				break;

			//Programs
			case 4:
				StartActivity (typeof(SessionChooserPrograms));

				break;

			}
			mDrawer.CloseDrawers ();
			mDrawerToggle.SyncState ();		
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {

			case Android.Resource.Id.Home:
				//The hamburger icon was clicked which means the drawer toggle will handle the event

				mDrawerToggle.OnOptionsItemSelected (item);
				return true;

			case Resource.Id.action_settings:
				//Refresh
				StartActivity (typeof(SettingsActivity));
				return true;


			default:
				return base.OnOptionsItemSelected (item);
			}
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.action_menu_settings, menu);
			return base.OnCreateOptionsMenu (menu);
		}

		protected override void OnPostCreate (Bundle savedInstanceState)
		{
			base.OnPostCreate (savedInstanceState);
			mDrawerToggle.SyncState ();
		}


		private void getPrefsId ()
		{

			prefs = AccountOperations.getSharedPreferences ();
			studentIdPrefs = prefs.GetString ("StudentIdNumber", String.Empty);
			thirtyMins = prefs.GetBoolean ("ReminderThirtyMins", false);
			tenMins = prefs.GetBoolean ("ReminderTenMins", false);
			oneDay = prefs.GetBoolean ("ReminderOneDay", false);
			oneWeek = prefs.GetBoolean ("ReminderOneWeek", false);
			editor = prefs.Edit ();

		}

	}

}
