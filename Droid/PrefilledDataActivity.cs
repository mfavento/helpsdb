﻿using System;
using Android.Preferences;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Json;
using System.Text;
using System.Collections.Specialized;
using Newtonsoft.Json;
using ModernHttpClient;
using Android.Content.PM;

namespace testDB.Droid
{
	[Activity (Label = "Register", ScreenOrientation = ScreenOrientation.Portrait)]

	public class PrefilledDataActivity : Activity
	{

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.prefilled_data);
			ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences (Application.Context); 
			//ISharedPreferencesEditor editor = prefs.Edit();
			//String studentIdPrefs = prefs.GetString ("StudentIdNumber", String.Empty);
			//finding fields...
			Button btnRegister = FindViewById<Button> (Resource.Id.btn_register);
			Button btnFalse = FindViewById<Button> (Resource.Id.btn_falseInformation);
			//getInfoFromDatabase()

			btnFalse.Click += (sender, e) => {

				StartActivity (typeof(StaticFalseInfo));


			};

			btnRegister.Click += (sender, e) => {

				StartActivity (typeof(RegisterStudent));

			};
						
		}


		/*
		 * This method provides HELPS staff with gathering information from the existing HELPS database
		 */

		public override void OnBackPressed ()
		{
			// This prevents a user from being able to hit the back button and leave the login page.
		}

		//this method will assist in getting information from the actual UTS database.
		//		private void getInfoFromDatabase(){
		//
		//			TextView txtStudent = FindViewById<TextView> (Resource.Id.txt_studentname);
		//			TextView txtFaculty = FindViewById<TextView> (Resource.Id.txt_faculty);
		//			TextView txtCourse = FindViewById<TextView> (Resource.Id.txt_course);
		//			TextView txtEmail = FindViewById<TextView> (Resource.Id.txt_email);
		//			TextView txtHomePhone = FindViewById<TextView> (Resource.Id.txt_homephone);
		//			TextView txtMobilePhone = FindViewById<TextView> (Resource.Id.txt_mobilephone);
		//			TextView txtDOB = FindViewById<TextView> (Resource.Id.txt_dob);
		//
		//
		//			//client.DownloadValuesAsync(uri)
		//		}
	
	}
}
