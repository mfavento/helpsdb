﻿using System;
using Android.Preferences;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Json;
using System.Text;
using System.Collections.Specialized;
using Newtonsoft.Json;
using ModernHttpClient;
using Android.Content.PM;

namespace testDB.Droid
{
	[Activity (Label = "Register", ScreenOrientation = ScreenOrientation.Portrait)]

	public class RegisterStudent : Activity
	{
		EditText etPrefName;
		String studentIdPrefs;
		ISharedPreferences prefs;



		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.register_student);
			//ISharedPreferencesEditor editor = prefs.Edit();

			getPrefsId ();
			//finding fields...
			Button btnRegister = FindViewById<Button> (Resource.Id.btn_register);
			etPrefName = FindViewById<EditText> (Resource.Id.et_prefName);


			RadioGroup rgroupGender = FindViewById<RadioGroup> (Resource.Id.rbg_gender);
			RadioButton rbtnGender = FindViewById<RadioButton> (rgroupGender.CheckedRadioButtonId);
			RadioGroup rgroupDegree = FindViewById<RadioGroup> (Resource.Id.rbg_degree);
			RadioButton rbtnDegree = FindViewById<RadioButton> (rgroupDegree.CheckedRadioButtonId);
			RadioGroup rgroupStatus = FindViewById<RadioGroup> (Resource.Id.rbg_status);
			RadioButton rbtnStatus = FindViewById<RadioButton> (rgroupStatus.CheckedRadioButtonId);

			Spinner spnLanguage = FindViewById<Spinner> (Resource.Id.spin_language);
			Spinner spnOrigin = FindViewById<Spinner> (Resource.Id.spin_origin);
			Spinner spnYear = FindViewById<Spinner> (Resource.Id.spin_year);

			//making adapters
			var spnYearAdapter = ArrayAdapter.CreateFromResource (this, Resource.Array.year_array, Android.Resource.Layout.SimpleSpinnerItem);
			var spnLanAdapter = ArrayAdapter.CreateFromResource (this, Resource.Array.languages_array, Android.Resource.Layout.SimpleSpinnerItem);
			var spnOriAdapter = ArrayAdapter.CreateFromResource (this, Resource.Array.origin_array, Android.Resource.Layout.SimpleSpinnerItem);

			//assigning adapters
			spnLanguage.Adapter = spnLanAdapter;
			spnOrigin.Adapter = spnOriAdapter;
			spnYear.Adapter = spnYearAdapter;

			var spnLanValue = spnLanguage.SelectedItemPosition;
			var spnOriValue = spnOrigin.SelectedItemPosition;
			var spnYearValue = spnYear.SelectedItemPosition;



			btnRegister.Click += (sender, e) => {

				var spnLanText = spnLanguage.GetItemAtPosition (spnLanValue).ToString ();
				var spnOriText = spnOrigin.GetItemAtPosition (spnOriValue).ToString ();
				var spnYearText = spnYear.GetItemAtPosition (spnYearValue).ToString ();


				//assign studentDetails object values

				WebClient client = new MyWebClient ();
				Uri uri = new Uri ("http://helpsdb.cloudapp.net/api/student/register");
				client.Headers ["AppKey"] = "999998";


				NameValueCollection parameters = new NameValueCollection ();
				parameters.Add ("StudentId", studentIdPrefs);
						
				//parameters.Add("Degree", rbtnDegree.Text);
				parameters.Add ("Status", rbtnStatus.Text);
				parameters.Add ("Gender", rbtnGender.Text);
				parameters.Add ("FirstLanguage", spnLanText);
				parameters.Add ("CountryOrigin", spnOriText);
				parameters.Add ("DegreeDetails", spnYearText);
				parameters.Add ("CreatorId", etPrefName.Text);
				client.UploadValuesCompleted += Client_UploadValuesCompleted;
				client.UploadValuesAsync (uri, parameters);
			};

		}

		void Client_UploadValuesCompleted (object sender, UploadValuesCompletedEventArgs e)
		{
			string result = Encoding.UTF8.GetString (e.Result);
			Console.WriteLine (result);
			setDefaultValues ();

			StartActivity (typeof(StaticRegistered));
		}

		private void setDefaultValues ()
		{
			ISharedPreferencesEditor editor = prefs.Edit ();
			editor.PutBoolean ("ReminderThirtyMins", true);              
			editor.PutBoolean ("ReminderOneDay", true);              
			editor.Commit ();
		}

		public override void OnBackPressed ()
		{
			// This prevents a user from being able to hit the back button and leave the login page.
		}


		private void getPrefsId ()
		{

			prefs = AccountOperations.getSharedPreferences ();
			studentIdPrefs = prefs.GetString ("StudentIdNumber", String.Empty);

		}

	}

}
