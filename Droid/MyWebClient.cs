﻿using System;
using System.Net;

namespace testDB.Droid
{
	public class MyWebClient : WebClient
	{
		protected override WebRequest GetWebRequest(Uri uri)
		{
			
			WebRequest w = base.GetWebRequest(uri);
			w.Proxy = null;
			w.Timeout = 1000;
			return w;
		}
	}

}