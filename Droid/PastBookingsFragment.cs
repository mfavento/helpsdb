﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Support.V7.Widget;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Android.Preferences;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.Widget;
using Javax.Crypto;

namespace testDB.Droid
{
	public class PastBookingsFragment : Fragment
	{
		ISharedPreferences prefs;
		View mView;
		RecyclerView mRecyclerView;
		RecyclerView.LayoutManager mLayoutManager;
		BookingSet mBookingSet;
		WorkshopPastAdapter mAdapter;
		SwipeRefreshLayout mSwipeRefreshLayout;


		public override void OnCreate (Bundle savedInstanceState)
		{
			getPrefsId ();
			base.OnCreate (savedInstanceState);
			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			mView = inflater.Inflate (Resource.Layout.PastWorkshopFragment, container, false);
			mRecyclerView = mView.FindViewById<RecyclerView> (Resource.Id.rcl_bookedWorkshops);
			mLayoutManager = new LinearLayoutManager (this.Activity);
			mRecyclerView.SetLayoutManager (mLayoutManager);
			mAdapter = new WorkshopPastAdapter (mBookingSet);
			mAdapter.ItemClick += OnItemClick;
			mRecyclerView.SetAdapter (mAdapter);
			swipeRefreshSetup ();
			if (mBookingSet.bookings.Count > 0) {
				TextView tvLabel = mView.FindViewById<TextView> (Resource.Id.tv_label);
				tvLabel.Text = "Click your past bookings below to view that booking.";

			}

			return mView;
					
		}

		public override void OnResume ()
		{
			base.OnResume ();

		}

		private void swipeRefreshSetup ()
		{
			mSwipeRefreshLayout = mView.FindViewById<SwipeRefreshLayout> (Resource.Id.swipeRefreshLayout);

			mSwipeRefreshLayout.Refresh += delegate {
				refreshAdapter ();
				mSwipeRefreshLayout.Refreshing = false;
			};
		}

		public void addListing (BookingSet bookingSet)
		{
			mBookingSet = bookingSet;

		}


		void OnItemClick (object sender, int position)
		{
			//show past bookings
					
		}




		public void refreshAdapter ()
		{
			var accountOverview = (AccountOverview)this.Activity;
			accountOverview.fetchPastBookings ();
			mAdapter.NotifyDataSetChanged ();
		

		}

		private void getPrefsId ()
		{

			prefs = PreferenceManager.GetDefaultSharedPreferences (Application.Context); 
		}


	}
}

