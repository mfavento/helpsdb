﻿using ModernHttpClient;
using Android.Support.V4.View;
using Android.Support.V4.Widget;

using Android.Preferences;
using System;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Json;
using System.Collections.Specialized;
using System.Collections;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Java.Util;
using Android.Provider;
using Android.Text.Format;
using Android.Content.PM;

namespace testDB.Droid
{
	[Activity (Label = "WorkshopActivity", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class WorkshopActivity : AppCompatActivity
	{
		//navigation variables
		private DrawerLayout mDrawer;
		private MyActionBarDrawerToggle mDrawerToggle;
		private ListView mDrawerList;
		private SupportToolbar mToolbar;
		private ArrayAdapter mAdapter;

		String studentIdPrefs;
		ISharedPreferences prefs;
		ProgressDialog progress;

		Workshop mWorkshop;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.workshop_activity);
			Button btnBook = FindViewById<Button> (Resource.Id.btn_book);
			string json = Intent.GetStringExtra ("model");



			mWorkshop = JsonConvert.DeserializeObject<Workshop> (json,
				new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy HH:mm" });
			
			getPrefsId ();

			Toast.MakeText (this, mWorkshop.Topic, ToastLength.Short).Show ();

		
			mToolbar = FindViewById<SupportToolbar> (Resource.Id.toolbar);
			SetSupportActionBar (mToolbar);
			setupNavDrawer ();

			btnBook.Click += delegate {
				Console.WriteLine (json);
				displayAlertBooking (mWorkshop.WorkshopId);
			};
			fillFields (mWorkshop);
		}

		long GetDateTimeMS (DateTime date)
		{

			Calendar c = Calendar.GetInstance (Java.Util.TimeZone.Default);
			c.Set (Calendar.DayOfMonth, date.Day);
			c.Set (Calendar.HourOfDay, date.Hour);
			c.Set (Calendar.Minute, date.Minute);
			c.Set (Calendar.Month, date.Month - 1);
			c.Set (Calendar.Year, date.Year);
			return c.TimeInMillis;
		}


		private void putReminderOnCalendar ()
		{

			var calID = GetCalendarId ();


			ContentValues eventValues = new ContentValues ();
			eventValues.Put (CalendarContract.Events.InterfaceConsts.CalendarId, calID);
			eventValues.Put (CalendarContract.Events.InterfaceConsts.Title, mWorkshop.Topic);
			eventValues.Put (CalendarContract.Events.InterfaceConsts.Description, mWorkshop.Description);
			eventValues.Put (CalendarContract.Events.InterfaceConsts.Dtstart, GetDateTimeMS (DateTime.ParseExact (mWorkshop.StartDate, "dd/MM/yyyy HH:mm", null)));
			eventValues.Put (CalendarContract.Events.InterfaceConsts.Dtend, GetDateTimeMS (DateTime.ParseExact (mWorkshop.EndDate, "dd/MM/yyyy HH:mm", null)));
			eventValues.Put (CalendarContract.Events.InterfaceConsts.AllDay, false);
			eventValues.Put (CalendarContract.Events.InterfaceConsts.EventTimezone, Time.CurrentTimezone);
			eventValues.Put (CalendarContract.Events.InterfaceConsts.EventEndTimezone, Time.CurrentTimezone);


			var eventUri = this.ContentResolver.Insert (CalendarContract.Events.ContentUri, eventValues);
			Console.WriteLine ("Uri for new event: {0}", eventUri);
			long eventID = long.Parse (eventUri.LastPathSegment);

			string reminderUriString = "content://com.android.calendar/reminders";
			Android.Net.Uri url = Android.Net.Uri.Parse (reminderUriString);

			if (prefs.GetBoolean ("ReminderThirtyMins", false))
				setReminder (30, eventID);
			if (prefs.GetBoolean ("ReminderTenMins", false))
				setReminder (10, eventID);
			if (prefs.GetBoolean ("ReminderOneDay", false))
				setReminder (1440, eventID);
			if (prefs.GetBoolean ("ReminderOneWeek", false))
				setReminder (10080, eventID);


		}


		private void setReminder (int minutes, long eventID)
		{
			ContentValues remindervalues = new ContentValues ();
			remindervalues.Put (CalendarContract.Reminders.InterfaceConsts.Minutes, minutes);
			remindervalues.Put (CalendarContract.Reminders.InterfaceConsts.EventId, eventID);
			remindervalues.Put (CalendarContract.Reminders.InterfaceConsts.Method, (int)Android.Provider.RemindersMethod.Alert);
			var uri = this.ContentResolver.Insert (CalendarContract.Reminders.ContentUri, remindervalues);


		}

		private void fillFields (Workshop workshop)
		{
			//works!
			TextView txtTopic = FindViewById<TextView> (Resource.Id.txt_topic);
			TextView txtDescrption = FindViewById<TextView> (Resource.Id.txt_description);
			TextView txtStart = FindViewById<TextView> (Resource.Id.txt_start);
			TextView txtFinish = FindViewById<TextView> (Resource.Id.txt_finish);
			TextView txtTime = FindViewById<TextView> (Resource.Id.txt_time);
			TextView txtPlaces = FindViewById<TextView> (Resource.Id.txt_places);
			TextView txtRoom = FindViewById<TextView> (Resource.Id.txt_room);
			TextView txtTargetGroup = FindViewById<TextView> (Resource.Id.txt_target_group);




			txtTopic.Text = workshop.Topic;
			txtDescrption.Text = workshop.Description;
			txtStart.Text = Intent.GetStringExtra ("startDate");
			txtFinish.Text = Intent.GetStringExtra ("endDate");
			txtTime.Text = Intent.GetStringExtra ("time");
			//txtNoSessions.Text = workshop.s

			txtPlaces.Text = "" + workshop.getPlacesAmt ();
			txtRoom.Text = workshop.Campus;
			txtTargetGroup.Text = workshop.TargetingGroup;
		
		}


		private void bookWorkshop (int id)
		{
			
			progress = getProgess ("Booking workshop...");

			progress.Show ();

			Uri uri = new Uri (GetString (Resource.String.service_domain) + "workshop/booking/create?workshopID=" + id + "&studentID=" + studentIdPrefs + "&userID=" + studentIdPrefs);

			var client = new WebClient ();
			client.Proxy = null;
			client.Headers ["AppKey"] = "999998";
			NameValueCollection parameters = new NameValueCollection ();
			//parameters.Add("workshopId", ""+id);
			parameters.Add ("studentId", studentIdPrefs);
			parameters.Add ("userId", studentIdPrefs);

			client.UploadValuesCompleted += Client_UploadDataCompleted;
			client.UploadValuesAsync (uri, parameters);

		}

		private void getPrefsId ()
		{

			prefs = AccountOperations.getSharedPreferences ();
			studentIdPrefs = prefs.GetString ("StudentIdNumber", String.Empty);

		}

		void Client_UploadDataCompleted (object sender, UploadValuesCompletedEventArgs e)
		{
			string result = Encoding.UTF8.GetString (e.Result);

			var json = (JObject)JsonConvert.DeserializeObject (result);
			bool success = (bool)json.SelectToken ("IsSuccess");
			//works!
			if (success) {
				Toast.MakeText (this, "Booking success!", ToastLength.Short).Show ();
				putReminderOnCalendar ();
				StartActivity (typeof(StaticBookingConfirmed));
			} else {
				Toast.MakeText (this, "Booking not successful, reason: " + result + "          ", ToastLength.Short).Show ();
			}


			progress.Dismiss ();
		}

		private ProgressDialog getProgess (String message)
		{
			progress = new ProgressDialog (this);
			progress.Indeterminate = true;
			progress.SetProgressStyle (ProgressDialogStyle.Spinner);
			progress.SetMessage (message);
			progress.SetCancelable (false);
			return progress;
		}



		private void setupNavDrawer ()
		{
			mDrawer = FindViewById<DrawerLayout> (Resource.Id.drawer_layout);
			mDrawerList = FindViewById<ListView> (Resource.Id.left_drawer);
			mDrawerList.Tag = 0;
			String[] navTitles = Resources.GetStringArray (Resource.Array.nav_array);

			mAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, navTitles);
			mDrawerList.Adapter = mAdapter;
			mDrawerList.ItemClick += (sender, args) => SelectItem (args.Position);


			mDrawerToggle = new MyActionBarDrawerToggle (
				this,
				mDrawer,
				0,
				0);

			mDrawer.SetDrawerListener (mDrawerToggle);
			SupportActionBar.SetHomeButtonEnabled (true);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);
			SupportActionBar.SetDisplayShowTitleEnabled (true);
			mDrawerToggle.SyncState ();
		}

		private void SelectItem (int position)
		{

			mDrawerList.SetItemChecked (position, true);

			switch (position) {

			//home
			case 0:
				StartActivity (typeof(AccountOverview));
				break;
			//settings
			case 1: 
				StartActivity (typeof(SettingsActivity));

				break;
			//make booking
			case 2:
				StartActivity (typeof(WorkshopChooser));

				break;
			//FAQ
			case 3:
				Toast.MakeText (this, "FAQ", ToastLength.Short).Show ();
				var uri = Android.Net.Uri.Parse ("https://helps-booking.uts.edu.au/index.cfm?scope=help");
				var intent = new Intent (Intent.ActionView, uri);
				StartActivity (intent);
				break;

			//Programs
			case 4:
				StartActivity (typeof(SessionChooserPrograms));
				break;

			}
			mDrawer.CloseDrawers ();
			mDrawerToggle.SyncState ();		
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {

			case Android.Resource.Id.Home:
				//The hamburger icon was clicked which means the drawer toggle will handle the event
				mDrawerToggle.OnOptionsItemSelected (item);
				return true;
			

			default:
				return base.OnOptionsItemSelected (item);
			}
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.action_menu_workshop, menu);
			return base.OnCreateOptionsMenu (menu);
		}

		protected override void OnPostCreate (Bundle savedInstanceState)
		{
			base.OnPostCreate (savedInstanceState);
			mDrawerToggle.SyncState ();
		}

		private void displayAlertBooking (int workshopId)
		{
			Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder (this);

			alert.SetTitle ("Book workshop?");

			alert.SetPositiveButton ("Book", (senderAlert, args) => {
				bookWorkshop (workshopId);
			});

			alert.SetNegativeButton ("Cancel", (senderAlert, args) => {
			});

			RunOnUiThread (() => {
				alert.Show ();
			});

		}


		int GetCalendarId ()
		{

			var calendarsUri = CalendarContract.Calendars.ContentUri;

			string[] calendarsProjection = {
				CalendarContract.Calendars.InterfaceConsts.Id,
				CalendarContract.Calendars.InterfaceConsts.CalendarDisplayName,
				CalendarContract.Calendars.InterfaceConsts.AccountName
			};

			AndroidUtils.Context = this;

			if (AndroidUtils.Context == null)
				return -1;

			var cursor = AndroidUtils.Context.ManagedQuery (calendarsUri, calendarsProjection, null, null, null);

			if (cursor.Count == 0)
				return -1;


			cursor.MoveToPosition (0);
			int calId = cursor.GetInt (cursor.GetColumnIndex (calendarsProjection [0]));
			return calId;
		}







	}
}
