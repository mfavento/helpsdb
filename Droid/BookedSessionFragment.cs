﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Support.V7.Widget;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Android.Preferences;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.Widget;
using Javax.Crypto;

namespace testDB.Droid
{
	public class BookedSessionFragment : Fragment
	{
		ISharedPreferences prefs;
		View mView;
		String studentIdPrefs;
		RecyclerView mRecyclerView;
		RecyclerView.LayoutManager mLayoutManager;
		SessionSet mSessionSet;
		SessionAdapter mAdapter;
		SwipeRefreshLayout mSwipeRefreshLayout;


		public override void OnCreate (Bundle savedInstanceState)
		{
			getPrefsId ();
			base.OnCreate (savedInstanceState);
			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			mView = inflater.Inflate (Resource.Layout.BookedSessionFragment, container, false);
			mRecyclerView = mView.FindViewById<RecyclerView> (Resource.Id.rcl_bookedWorkshops);
			mLayoutManager = new LinearLayoutManager (this.Activity);
			mRecyclerView.SetLayoutManager (mLayoutManager);
			mAdapter = new SessionAdapter (mSessionSet);
			mAdapter.ItemClick += OnItemClick;
			mRecyclerView.SetAdapter (mAdapter);
			swipeRefreshSetup ();
			if (mSessionSet.sessions.Count > 0) {
				TextView tvLabel = mView.FindViewById<TextView> (Resource.Id.tv_label);
				tvLabel.Text = "These are your current sessions:";

			}
			mAdapter.NotifyDataSetChanged ();

			return mView;

		}

		public override void OnResume ()
		{
			base.OnResume ();

		}

		private void swipeRefreshSetup ()
		{
			mSwipeRefreshLayout = mView.FindViewById<SwipeRefreshLayout> (Resource.Id.swipeRefreshLayout);

			mSwipeRefreshLayout.Refresh += delegate {
				refreshAdapter ();
				mSwipeRefreshLayout.Refreshing = false;
			};
		}

		public void addListing (SessionSet sessionSet)
		{
			mSessionSet = sessionSet;

		}


		void OnItemClick (object sender, int position)
		{
			var accountOverview = (AccountOverview)this.Activity;
			DateTime endDate = DateTime.ParseExact (mSessionSet.sessions [position].EndDate, "dd/MM/yyyy HH:mm", null);

			if (endDate < DateTime.Now)
				accountOverview.viewSessionAttendanceFragment (mSessionSet.sessions [position]);
			else
				accountOverview.viewSessionFragment (mSessionSet.sessions [position]);

		}






		public void refreshAdapter ()
		{
			var accountOverview = (AccountOverview)this.Activity;
			accountOverview.fetchSessionBookings ();
			mAdapter.NotifyDataSetChanged ();


		}

		private void getPrefsId ()
		{

			prefs = AccountOperations.getSharedPreferences ();
			studentIdPrefs = prefs.GetString ("StudentIdNumber", String.Empty);

		}


	}
}

