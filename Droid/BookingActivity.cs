﻿using ModernHttpClient;

using Android.Preferences;
using System;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Json;
using System.Collections.Specialized;
using System.Collections;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Content.PM;



namespace testDB.Droid
{
	[Activity (Label = "BookingActivity", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class BookingActivity : AppCompatActivity
	{

		String studentIdPrefs;
		ISharedPreferences prefs;
		ProgressDialog progress;

		private DrawerLayout mDrawer;
		private MyActionBarDrawerToggle mDrawerToggle;
		private ListView mDrawerList;
		private SupportToolbar mToolbar;
		private ArrayAdapter mAdapter;


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.booking_activity);
			Button btnCancel = FindViewById<Button> (Resource.Id.btn_cancel);
			string json = Intent.GetStringExtra ("model");

			Booking booking = JsonConvert.DeserializeObject<Booking> (json,
				                  new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
			

			getPrefsId ();
			Toast.MakeText (this, booking.Topic, ToastLength.Short).Show ();

			mToolbar = FindViewById<SupportToolbar> (Resource.Id.toolbar);
			SetSupportActionBar (mToolbar);
			setupNavDrawer ();

			btnCancel.Click += delegate {
				displayAlertBooking (booking);
			};
			fillFields (booking);
		}

		private void displayAlertBooking (Booking booking)
		{
			Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder (this);

			alert.SetTitle ("Cancel this Workshop?");

			alert.SetPositiveButton ("Yes", (senderAlert, args) => {
				DateTime start = DateTime.Parse (booking.StartDate);

				//within 24 hours
				if (start > DateTime.Now.AddHours (-24) && start <= DateTime.Now) {
					UpdateHelper.cancelBookingStrike (this, booking);
					Console.WriteLine ("***\n***Cancelled booking has boolean to show that user has cancelled within 24 hours***\n***");
					StartActivity (typeof(StaticCanceled));

				} else {
					UpdateHelper.archiveBooking (this, booking);
					Console.WriteLine ("***\n***More than 24 hours, set as archived***\n***");
					StartActivity (typeof(StaticCanceled));
				}

			});

			alert.SetNegativeButton ("No", (senderAlert, args) => {
			});

			RunOnUiThread (() => {
				alert.Show ();
			});

		}

		private void cancelBookingStrike (Booking booking)
		{
			UpdateHelper.cancelBookingStrike (this, booking);
		}

		void Client_UploadValuesCompleted (object sender, UploadValuesCompletedEventArgs e)
		{
			string result = Encoding.UTF8.GetString (e.Result);
			Console.WriteLine (result);
			StartActivity (typeof(StaticCanceled));
		}


		private void fillFields (Booking booking)
		{
			//works!
			TextView txtTopic = FindViewById<TextView> (Resource.Id.txt_topic);
			TextView txtDescription = FindViewById<TextView> (Resource.Id.txt_description);
			TextView txtStart = FindViewById<TextView> (Resource.Id.txt_start);
			TextView txtFinish = FindViewById<TextView> (Resource.Id.txt_finish);
			TextView txtTime = FindViewById<TextView> (Resource.Id.txt_time);
			TextView txtRoom = FindViewById<TextView> (Resource.Id.txt_room);
			TextView txtCompleted = FindViewById<TextView> (Resource.Id.txt_completed);
			String completedConvert = "";
			Console.WriteLine (booking.Topic + " " + booking.StartDate + " time: " + booking.Time);


			txtTopic.Text = booking.Topic;
			txtDescription.Text = booking.Description;
			txtStart.Text = Intent.GetStringExtra ("startDate");
			txtFinish.Text = Intent.GetStringExtra ("endDate");
			txtTime.Text = Intent.GetStringExtra ("time");
			txtRoom.Text = "" + booking.Campus;

			if (booking.BookingArchived != null) {
				completedConvert = "Yes";
			} else {
				completedConvert = "No";
			}


			txtCompleted.Text = completedConvert;

		}


		private void getPrefsId ()
		{

			prefs = PreferenceManager.GetDefaultSharedPreferences (Application.Context); 
			studentIdPrefs = prefs.GetString ("StudentIdNumber", String.Empty);

		}


		private ProgressDialog getProgess (String message)
		{
			progress = new ProgressDialog (this);
			progress.Indeterminate = true;
			progress.SetProgressStyle (ProgressDialogStyle.Spinner);
			progress.SetMessage (message);
			progress.SetCancelable (false);
			return progress;
		}

		private void setupNavDrawer ()
		{
			mDrawer = FindViewById<DrawerLayout> (Resource.Id.drawer_layout);
			mDrawerList = FindViewById<ListView> (Resource.Id.left_drawer);
			mDrawerList.Tag = 0;
			String[] navTitles = Resources.GetStringArray (Resource.Array.nav_array);

			mAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, navTitles);
			mDrawerList.Adapter = mAdapter;
			mDrawerList.ItemClick += (sender, args) => SelectItem (args.Position);


			mDrawerToggle = new MyActionBarDrawerToggle (
				this,
				mDrawer,
				0,
				0);

			mDrawer.SetDrawerListener (mDrawerToggle);
			SupportActionBar.SetHomeButtonEnabled (true);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);
			SupportActionBar.SetDisplayShowTitleEnabled (true);
			mDrawerToggle.SyncState ();
		}

		private void SelectItem (int position)
		{

			mDrawerList.SetItemChecked (position, true);

			switch (position) {

			//home
			case 0:
				StartActivity (typeof(AccountOverview));
				break;
			//settings
			case 1: 
				StartActivity (typeof(SettingsActivity));

				break;
			//make booking
			case 2:
				StartActivity (typeof(WorkshopChooser));

				break;
			//FAQ
			case 3:
				Toast.MakeText (this, "FAQ", ToastLength.Short).Show ();
				var uri = Android.Net.Uri.Parse ("https://helps-booking.uts.edu.au/index.cfm?scope=help");
				var intent = new Intent (Intent.ActionView, uri);
				StartActivity (intent);
				break;

			//Programs
			case 4:
				StartActivity (typeof(SessionChooserPrograms));
				break;

			}
			mDrawer.CloseDrawers ();
			mDrawerToggle.SyncState ();		
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {

			case Android.Resource.Id.Home:
				//The hamburger icon was clicked which means the drawer toggle will handle the event
				mDrawerToggle.OnOptionsItemSelected (item);
				return true;


			default:
				return base.OnOptionsItemSelected (item);
			}
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.action_menu_workshop, menu);
			return base.OnCreateOptionsMenu (menu);
		}

		protected override void OnPostCreate (Bundle savedInstanceState)
		{
			base.OnPostCreate (savedInstanceState);
			mDrawerToggle.SyncState ();
		}



	}
}
