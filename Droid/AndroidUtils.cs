﻿using System;
using Android.App;
using Android.Views;

namespace testDB.Droid
{
	public static partial class AndroidUtils
	{
		public static Activity Context { get; set; }

		public static View SnackbarView { get; set; }
	}
}
