﻿using System;
using System.Collections.Generic;
using Java.Lang;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace testDB.Droid
{
	public class Booking
	{
		public int BookingId{ get; private set; }

		public int WorkshopId{ get; private set; }

		public int StudentId{ get; private set; }

		public string Topic{ get; private set; }

		public string Description{ get; private set; }

		public string TargetingGroup { get; private set; }

		public int Campus { get; private set; }

		public string StartDate { get; private set; }

		public string EndDate { get; private set; }

		public int Maximum { get; private set; }

		public int? Canceled { get; private set; }

		public int? Attended { get; private set; }


		public int Cutoff { get; private set; }

		public string Time { get; private set; }

		public int WorkShopSetID { get; private set; }

		public string Type { get; private set; }

		public int ReminderNum { get; private set; }

		public int ReminderSent { get; private set; }

		public DateTime WorkshopArchived { get; private set; }

		public DateTime? BookingArchived { get; private set; }


		public Booking (int? canceled, int? attended, int studentId, int workshopId, string topic, string description, DateTime starting, DateTime ending, DateTime? bookingArchived, int campus)
		{
			Attended = attended;
			Canceled = canceled;
			StudentId = studentId;
			WorkshopId = workshopId;
			Topic = topic;
			Description = description;
			StartDate = getDate (starting);
			EndDate = getDate (ending);
			BookingArchived = bookingArchived;
			Campus = campus;
			Time = getTime (starting, ending);
		}

		private string getTime (DateTime starting, DateTime ending)
		{
			string returnTime = starting.TimeOfDay + " - " + ending.TimeOfDay;
			return returnTime;

		}

		private string getDate (DateTime dateTime)
		{
			string returnDate = dateTime.ToString ("dd/MM/yyyy HH:mm");
			return returnDate;

		}
	



	}

	public class BookingSet : Java.Lang.Object
	{
		public int StudentId{ get; private set; }

		public List<Booking> bookings = new List<Booking> ();
		public List<Booking> pastBookings = new List<Booking> ();

		public BookingSet ()
		{

		}

	}
}

