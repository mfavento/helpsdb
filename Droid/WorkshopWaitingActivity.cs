﻿using ModernHttpClient;

using Android.Preferences;
using System;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Json;
using System.Collections.Specialized;
using System.Collections;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using Android.Support.V4.Widget;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Content.PM;



namespace testDB.Droid
{
	[Activity (Label = "Add Workshop To Waiting List", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class WorkshopWaitingActivity : AppCompatActivity
	{

		private DrawerLayout mDrawer;
		private MyActionBarDrawerToggle mDrawerToggle;
		private ListView mDrawerList;
		private SupportToolbar mToolbar;
		private ArrayAdapter mAdapter;

		String studentIdPrefs;
		ISharedPreferences prefs;
		ProgressDialog progress;


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.workshop_waiting_activity);
			Button btnBook = FindViewById<Button> (Resource.Id.btn_book);
			string json = Intent.GetStringExtra ("model");
			Workshop workshop = JsonConvert.DeserializeObject<Workshop> (json,
				                    new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy HH:mm" });
			
			getPrefsId ();
			Toast.MakeText (this, workshop.Topic, ToastLength.Short).Show ();

			mToolbar = FindViewById<SupportToolbar> (Resource.Id.toolbar);
			SetSupportActionBar (mToolbar);
			setupNavDrawer ();

			btnBook.Click += delegate {
				Console.WriteLine (json);

				displayAlertBooking (workshop.WorkshopId);
			};
			fillFields (workshop);
		}


		private void fillFields (Workshop workshop)
		{
			//works!
			TextView txtTopic = FindViewById<TextView> (Resource.Id.txt_topic);
			TextView txtDescrption = FindViewById<TextView> (Resource.Id.txt_description);
			TextView txtStart = FindViewById<TextView> (Resource.Id.txt_start);
			TextView txtFinish = FindViewById<TextView> (Resource.Id.txt_finish);
			TextView txtTime = FindViewById<TextView> (Resource.Id.txt_time);
			TextView txtPlaces = FindViewById<TextView> (Resource.Id.txt_places);
			TextView txtRoom = FindViewById<TextView> (Resource.Id.txt_room);
			TextView txtTargetGroup = FindViewById<TextView> (Resource.Id.txt_target_group);




			txtTopic.Text = workshop.Topic;
			txtDescrption.Text = workshop.Description;
			txtStart.Text = Intent.GetStringExtra ("startDate");
			txtFinish.Text = Intent.GetStringExtra ("endDate");
			txtTime.Text = Intent.GetStringExtra ("time");
			//txtNoSessions.Text = workshop.s
			txtPlaces.Text = "Full";
			txtRoom.Text = workshop.Campus;
			txtTargetGroup.Text = workshop.TargetingGroup;
		
		}

		private void displayAlertBooking (int workshopId)
		{
			Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder (this);

			alert.SetTitle ("Add to Wait list?");

			alert.SetPositiveButton ("Add", (senderAlert, args) => {
				addToWaitlist (workshopId);
			});

			alert.SetNegativeButton ("Cancel", (senderAlert, args) => {
			});

			RunOnUiThread (() => {
				alert.Show ();
			});

		}

		private void addToWaitlist (int id)
		{
			
			progress = getProgess ("Adding workshop to wait list...");

			progress.Show ();

			//https://helpsdb.cloudapp.net/api/workshop/wait/create?workshopId=123&studentId=666&userId=007

			Uri uri = new Uri (GetString (Resource.String.service_domain) + "workshop/wait/create?workshopID=" + id + "&studentID=" + studentIdPrefs + "&userID=" + studentIdPrefs);

			var client = new WebClient ();
			client.Proxy = null;
			client.Headers ["AppKey"] = "999998";
			NameValueCollection parameters = new NameValueCollection ();
			//parameters.Add("workshopId", ""+id);
			parameters.Add ("studentId", studentIdPrefs);
			parameters.Add ("userId", studentIdPrefs);

			client.UploadValuesCompleted += Client_UploadDataCompleted;
			client.UploadValuesAsync (uri, parameters);

		}

		private void getPrefsId ()
		{

			prefs = PreferenceManager.GetDefaultSharedPreferences (Application.Context); 
			studentIdPrefs = prefs.GetString ("StudentIdNumber", String.Empty);

		}

		void Client_UploadDataCompleted (object sender, UploadValuesCompletedEventArgs e)
		{

			Toast.MakeText (this, "Added to Waitlist!", ToastLength.Short).Show ();
			progress.Dismiss ();
			StartActivity (typeof(StaticWaitlistConfirmed));
		}

		private ProgressDialog getProgess (String message)
		{
			progress = new ProgressDialog (this);
			progress.Indeterminate = true;
			progress.SetProgressStyle (ProgressDialogStyle.Spinner);
			progress.SetMessage (message);
			progress.SetCancelable (false);
			return progress;
		}

		private void setupNavDrawer ()
		{
			mDrawer = FindViewById<DrawerLayout> (Resource.Id.drawer_layout);
			mDrawerList = FindViewById<ListView> (Resource.Id.left_drawer);
			mDrawerList.Tag = 0;
			String[] navTitles = Resources.GetStringArray (Resource.Array.nav_array);

			mAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, navTitles);
			mDrawerList.Adapter = mAdapter;
			mDrawerList.ItemClick += (sender, args) => SelectItem (args.Position);


			mDrawerToggle = new MyActionBarDrawerToggle (
				this,
				mDrawer,
				0,
				0);

			mDrawer.SetDrawerListener (mDrawerToggle);
			SupportActionBar.SetHomeButtonEnabled (true);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);
			SupportActionBar.SetDisplayShowTitleEnabled (true);
			mDrawerToggle.SyncState ();
		}

		private void SelectItem (int position)
		{

			mDrawerList.SetItemChecked (position, true);

			switch (position) {

			//home
			case 0:
				StartActivity (typeof(AccountOverview));
				break;
			//settings
			case 1: 
				StartActivity (typeof(SettingsActivity));

				break;
			//make booking
			case 2:
				StartActivity (typeof(WorkshopChooser));

				break;
			//FAQ
			case 3:
				Toast.MakeText (this, "FAQ", ToastLength.Short).Show ();
				var uri = Android.Net.Uri.Parse ("https://helps-booking.uts.edu.au/index.cfm?scope=help");
				var intent = new Intent (Intent.ActionView, uri);
				StartActivity (intent);
				break;

			//Programs
			case 4:
				StartActivity (typeof(SessionChooserPrograms));
				break;

			}
			mDrawer.CloseDrawers ();
			mDrawerToggle.SyncState ();		
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {

			case Android.Resource.Id.Home:
				//The hamburger icon was clicked which means the drawer toggle will handle the event
				mDrawerToggle.OnOptionsItemSelected (item);
				return true;


			default:
				return base.OnOptionsItemSelected (item);
			}
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.action_menu_workshop, menu);
			return base.OnCreateOptionsMenu (menu);
		}

		protected override void OnPostCreate (Bundle savedInstanceState)
		{
			base.OnPostCreate (savedInstanceState);
			mDrawerToggle.SyncState ();
		}



	}
}
