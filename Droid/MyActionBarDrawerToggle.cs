﻿using System;
using SupportActionBarDrawerToggle = Android.Support.V7.App.ActionBarDrawerToggle;
using Android.Support.V7.App;
using Android.Support.V4.Widget;

namespace testDB.Droid
{
	public class MyActionBarDrawerToggle : SupportActionBarDrawerToggle
	{


		public MyActionBarDrawerToggle (AppCompatActivity hostActivity, DrawerLayout drawerLayout, int openResource, int closeResource) 
			: base(hostActivity, drawerLayout, openResource, closeResource)
		{

		}

		public override void OnDrawerOpened (Android.Views.View drawerView)
		{ 
			int drawerType = (int)drawerView.Tag;

			if (drawerType == 0)
			{
				base.OnDrawerOpened (drawerView);
			}
		}

		public override void OnDrawerClosed (Android.Views.View drawerView)
		{
			int drawerType = (int)drawerView.Tag;

			if (drawerType == 0)
			{
				base.OnDrawerClosed (drawerView);
			} 
		}

		public override void OnDrawerSlide (Android.Views.View drawerView, float slideOffset)
		{
			int drawerType = (int)drawerView.Tag;

			if (drawerType == 0)
			{
				base.OnDrawerSlide (drawerView, slideOffset);
			}
		}
	}
}