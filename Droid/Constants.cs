﻿using System;

namespace testDB.Droid
{
	public class Constants
	{
		public const string DB_URL = "http://helpsdb.cloudapp.net/";
		public const int SORT_DATE = 1;
		public const int SORT_LOCATION = 2;
		public const int SORT_TOPIC = 3;

		public const int SORT_TYPE = 5;
		public const int SORT_LECTURER = 6;

		public const int TEN_MINS = 1;
		public const int THIRTY_MINS = 2;
		public const int ONE_DAY = 3;
		public const int ONE_WEEK = 4;

		public const bool FUTURE_WORKSHOPS_ONLY = false;

		public const int SERVICE_DOMAIN = Resource.String.service_domain;

	}
}

