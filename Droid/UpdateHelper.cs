﻿using System;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Android.Content;
using System.Text;
using System.Net;
using System.Collections.Specialized;
using Newtonsoft.Json;
using Java.Lang;
using Android.Widget;

namespace testDB.Droid
{
	public class UpdateHelper
	{
		public UpdateHelper ()
		{


		}


		public static void cancelBookingStrike (Context context, Booking booking)
		{

			var client = new MyWebClient ();
			Uri uri = new Uri (context.GetString (Resource.String.service_domain) + "workshop/booking/update");
			client.Headers ["AppKey"] = "999998";
			client.Headers.Add ("Content-Type", "application/json");


			JObject rss = new JObject (
				              new JProperty ("WorkshopId", booking.WorkshopId),
				              new JProperty ("StudentId", booking.StudentId),
				              new JProperty ("Canceled", 1),
				              new JProperty ("UserId", booking.StudentId));

			string jsonString = JsonConvert.SerializeObject (rss);


			client.UploadStringCompleted += (sender, e) => { 

				archiveBooking (context, booking);
			};

			client.UploadStringAsync (uri, "PUT", jsonString);
		}

		public static void cancelSession (Context context, Session session, string userId)
		{

			var client = new MyWebClient ();
			Uri uri = new Uri (context.GetString (Resource.String.service_domain) + "session/booking/update");
			client.Headers ["AppKey"] = "999998";
			client.Headers.Add ("Content-Type", "application/json");


			JObject rss = new JObject (
				              new JProperty ("SessionId", session.SessionId),
				              new JProperty ("StudentId", userId),
				              new JProperty ("BookingId", session.BookingId),
				              new JProperty ("Canceled", 1),
				              new JProperty ("UserId", userId));

			string jsonString = JsonConvert.SerializeObject (rss);


			client.UploadStringCompleted += (sender, e) => { 

			};

			client.UploadStringAsync (uri, "PUT", jsonString);
		}

		public static void archiveBooking (Context context, Booking booking)
		{
			WebClient client = new WebClient ();

			Uri uri = new Uri ("http://helpsdb.cloudapp.net/api/workshop/booking/cancel?workshopId=" + booking.WorkshopId + "&studentId=" + booking.StudentId + "&userId=" + booking.StudentId);
			client.Headers ["AppKey"] = "999998";
			NameValueCollection parameters = new NameValueCollection ();
			client.UploadValuesAsync (uri, parameters);
			Console.WriteLine ("\n\n\n\n\n\n\n\n" + uri.ToString ());
			client.UploadValuesCompleted += (sender, e) => { 
				string result = Encoding.UTF8.GetString (e.Result);
				Console.WriteLine (result);
			};

		}


		public static void attendSession (Context context, Session session, string reason, string userId)
		{

			var client = new MyWebClient ();
			Uri uri = new Uri (context.GetString (Resource.String.service_domain) + "session/booking/update");
			client.Headers ["AppKey"] = "999998";
			client.Headers.Add ("Content-Type", "application/json");


			JObject rss = new JObject (
				              new JProperty ("SessionId", session.SessionId),
				              new JProperty ("StudentId", userId),
				              new JProperty ("BookingId", session.BookingId),
				              new JProperty ("Attended", 1),
				              new JProperty ("Reason", reason),
				              new JProperty ("UserId", userId));

			Console.WriteLine (reason + " " + userId);

			string jsonString = JsonConvert.SerializeObject (rss);


			client.UploadStringCompleted += (sender, e) => { 
				Console.WriteLine (e.Result.ToString ()); 
				client.Dispose ();

			};

			client.UploadStringAsync (uri, "PUT", jsonString);
		}

		public static void attendBooking (Context context, Booking booking)
		{

			var client = new MyWebClient ();
			Uri uri = new Uri (context.GetString (Resource.String.service_domain) + "workshop/booking/update");
			client.Headers ["AppKey"] = "999998";
			client.Headers.Add ("Content-Type", "application/json");


			JObject rss = new JObject (
				              new JProperty ("WorkshopId", booking.WorkshopId),
				              new JProperty ("StudentId", booking.StudentId),
				              new JProperty ("Attended", 1),
				              new JProperty ("UserId", booking.StudentId));

			string jsonString = JsonConvert.SerializeObject (rss);


			client.UploadStringCompleted += (sender, e) => { 
				Console.WriteLine (e.Result.ToString ()); 
				client.Dispose ();
				archiveBooking (context, booking);


			};

			client.UploadStringAsync (uri, "PUT", jsonString);
		}


	}
}



