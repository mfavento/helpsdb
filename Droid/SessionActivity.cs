﻿using ModernHttpClient;

using Android.Preferences;
using System;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Json;
using System.Collections.Specialized;
using System.Collections;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Android.Content.PM;
using Android.Support.V4.Widget;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;


namespace testDB.Droid
{
	[Activity (Label = "Session View", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class SessionActivity : AppCompatActivity
	{
		//navigation variables
		private DrawerLayout mDrawer;
		private MyActionBarDrawerToggle mDrawerToggle;
		private ListView mDrawerList;
		private SupportToolbar mToolbar;
		private ArrayAdapter mAdapter;

		ProgressDialog progress;


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.session_activity);
			Button btnCancel = FindViewById<Button> (Resource.Id.btn_cancel);
			string json = Intent.GetStringExtra ("model");

			Session session = JsonConvert.DeserializeObject<Session> (json,
				                  new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy HH:mm" });

			mToolbar = FindViewById<SupportToolbar> (Resource.Id.toolbar);
			SetSupportActionBar (mToolbar);
			setupNavDrawer ();


			fillFields (session);
		}



		private void fillFields (Session session)
		{
			//works!
			TextView txtLecturerName = FindViewById<TextView> (Resource.Id.txt_lec_name);
			TextView txtLecturerEmail = FindViewById<TextView> (Resource.Id.txt_lec_email);
			TextView txtSessionType = FindViewById<TextView> (Resource.Id.txt_session_type);
			TextView txtStart = FindViewById<TextView> (Resource.Id.txt_start);
			TextView txtFinish = FindViewById<TextView> (Resource.Id.txt_finish);
			TextView txtTime = FindViewById<TextView> (Resource.Id.txt_time);
			TextView txtLecComment = FindViewById<TextView> (Resource.Id.txt_lec_comment);


			txtLecturerName.Text = session.LecturerFirstName + " " + session.LecturerLastName;
			txtLecturerEmail.Text = session.LecturerEmail;
			txtSessionType.Text = session.SessionTypeAbb;
			txtStart.Text = Intent.GetStringExtra ("startDate");
			txtFinish.Text = Intent.GetStringExtra ("endDate");
			txtTime.Text = Intent.GetStringExtra ("time");
			txtLecComment.Text = session.LecturerComment;


		}



		private ProgressDialog getProgess (String message)
		{
			progress = new ProgressDialog (this);
			progress.Indeterminate = true;
			progress.SetProgressStyle (ProgressDialogStyle.Spinner);
			progress.SetMessage (message);
			progress.SetCancelable (false);
			return progress;
		}

		private void setupNavDrawer ()
		{
			mDrawer = FindViewById<DrawerLayout> (Resource.Id.drawer_layout);
			mDrawerList = FindViewById<ListView> (Resource.Id.left_drawer);
			mDrawerList.Tag = 0;
			String[] navTitles = Resources.GetStringArray (Resource.Array.nav_array);

			mAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, navTitles);
			mDrawerList.Adapter = mAdapter;
			mDrawerList.ItemClick += (sender, args) => SelectItem (args.Position);


			mDrawerToggle = new MyActionBarDrawerToggle (
				this,
				mDrawer,
				0,
				0);

			mDrawer.SetDrawerListener (mDrawerToggle);
			SupportActionBar.SetHomeButtonEnabled (true);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);
			SupportActionBar.SetDisplayShowTitleEnabled (true);
			mDrawerToggle.SyncState ();
		}

		private void SelectItem (int position)
		{

			mDrawerList.SetItemChecked (position, true);

			switch (position) {

			//home
			case 0:
				StartActivity (typeof(AccountOverview));
				break;
			//settings
			case 1: 
				StartActivity (typeof(SettingsActivity));

				break;
			//make booking
			case 2:
				StartActivity (typeof(WorkshopChooser));

				break;
			//FAQ
			case 3:
				Toast.MakeText (this, "FAQ", ToastLength.Short).Show ();
				var uri = Android.Net.Uri.Parse ("https://helps-booking.uts.edu.au/index.cfm?scope=help");
				var intent = new Intent (Intent.ActionView, uri);
				StartActivity (intent);
				break;

			//Programs
			case 4:
				StartActivity (typeof(SessionChooserPrograms));
				break;

			}
			mDrawer.CloseDrawers ();
			mDrawerToggle.SyncState ();		
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {

			case Android.Resource.Id.Home:
				//The hamburger icon was clicked which means the drawer toggle will handle the event
				mDrawerToggle.OnOptionsItemSelected (item);
				return true;


			default:
				return base.OnOptionsItemSelected (item);
			}
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.action_menu_workshop, menu);
			return base.OnCreateOptionsMenu (menu);
		}

		protected override void OnPostCreate (Bundle savedInstanceState)
		{
			base.OnPostCreate (savedInstanceState);
			mDrawerToggle.SyncState ();
		}



	}
}
