﻿using Android.Support.V4.Widget;
using System;
using System.Collections.Generic;
using Newtonsoft.Json.Converters;
using Android.Support.V4.View;
using System.Linq;
using System.Text;
using Android.Preferences;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Content.PM;

namespace testDB.Droid
{
	[Activity (Label = "Account Overview", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class AccountOverview : AppCompatActivity
	{
		//navigation variables
		private DrawerLayout mDrawer;
		private MyActionBarDrawerToggle mDrawerToggle;
		private ListView mDrawerList;
		private SupportToolbar mToolbar;
		private ArrayAdapter mAdapter;
		private BookingSet bookingSet;
		private SessionSet sessionSet;
		private ProgressBar mProgress;



		ISharedPreferences prefs;
		String studentIdPrefs;
		private ProgressDialog progress;


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.account_overview);
			mToolbar = FindViewById<SupportToolbar> (Resource.Id.toolbar);
			SetSupportActionBar (mToolbar);

			setupNavDrawer ();

			prefs = AccountOperations.getSharedPreferences ();
			studentIdPrefs = prefs.GetString ("StudentIdNumber", String.Empty);

			mProgress = FindViewById<ProgressBar> (Resource.Id.pb_loading);
			mProgress.Visibility = ViewStates.Visible;

			Button btnBook = FindViewById<Button> (Resource.Id.btn_book);
			btnBook.Click += delegate {
				StartActivity (typeof(WorkshopChooser));
			};

			fetchWorkshopBookings ();

			//fragment 'BookedSessionFragment'
			//fillSessionFragment();
			//fillPastFragment
		}

		public override void OnBackPressed ()
		{
			// This prevents a user from being able to hit the back button and leave the login page.
		}

		public void fillWorkshopFragment (BookingSet bookingSet)
		{
			//get studentId from input
			var bookedWorkshopFragment = new BookedWorkshopFragment ();
			bookedWorkshopFragment.addListing (bookingSet);
			var ft = FragmentManager.BeginTransaction ();
			mProgress.Visibility = ViewStates.Gone;

			ft.Replace (Resource.Id.fragment_workshop_container, bookedWorkshopFragment, "WORKSHOPFRAG");
			ft.CommitAllowingStateLoss ();
		}

		public void fillPastFragment (BookingSet bookingSet)
		{
			var bookedWorkshopFragment = new PastBookingsFragment ();
			bookedWorkshopFragment.addListing (bookingSet);
			var ft = FragmentManager.BeginTransaction ();
			mProgress.Visibility = ViewStates.Gone;

			ft.Replace (Resource.Id.fragment_workshop_container, bookedWorkshopFragment, "PASTFRAG");
			ft.CommitAllowingStateLoss ();
		}

		public void fillSessionFragment (SessionSet sessionSet)
		{
			var sessionsFragment = new BookedSessionFragment ();
			sessionsFragment.addListing (sessionSet);
			var ft = FragmentManager.BeginTransaction ();
			mProgress.Visibility = ViewStates.Gone;

			ft.Replace (Resource.Id.fragment_workshop_container, sessionsFragment, "SESHFRAG");
			ft.CommitAllowingStateLoss ();
		}


		public void viewBookingFragment (Booking booking)
		{
			var intent = new Intent (this, typeof(BookingActivity));
			string jsonModel = JsonConvert.SerializeObject (booking, new IsoDateTimeConverter ());
			intent.PutExtra ("time", booking.Time);
			intent.PutExtra ("startDate", booking.StartDate);
			intent.PutExtra ("endDate", booking.EndDate);
			intent.PutExtra ("model", jsonModel);
			StartActivity (intent);
		}


		public void viewSessionFragment (Session session)
		{
			var intent = new Intent (this, typeof(SessionActivity));
			string jsonModel = JsonConvert.SerializeObject (session, new IsoDateTimeConverter ());
			intent.PutExtra ("time", session.Time);
			intent.PutExtra ("startDate", session.StartDate);
			intent.PutExtra ("endDate", session.EndDate);
			intent.PutExtra ("model", jsonModel);
			StartActivity (intent);
		}

		public void viewAttendanceFragment (Booking booking)
		{
			var intent = new Intent (this, typeof(AttendanceActivity));
			string jsonModel = JsonConvert.SerializeObject (booking, new IsoDateTimeConverter ());
			intent.PutExtra ("time", booking.Time);
			intent.PutExtra ("startDate", booking.StartDate);
			intent.PutExtra ("endDate", booking.EndDate);
			intent.PutExtra ("model", jsonModel);
			StartActivity (intent);
		}

		public void viewSessionAttendanceFragment (Session session)
		{
			var intent = new Intent (this, typeof(AttendanceSessionActivity));
			string jsonModel = JsonConvert.SerializeObject (session, new IsoDateTimeConverter ());
			intent.PutExtra ("time", session.Time);
			intent.PutExtra ("startDate", session.StartDate);
			intent.PutExtra ("endDate", session.EndDate);
			intent.PutExtra ("model", jsonModel);
			StartActivity (intent);
		}


		public void fetchSessions ()
		{


		}


		public void fetchWorkshopBookings ()
		{
			progress = getProgess ("Searching for bookings...");

			Uri uri = new Uri (GetString (Resource.String.service_domain) + "workshop/booking/search?pageSize=50&studentId=" + studentIdPrefs);
			var client = new MyWebClient ();
			client.Headers ["AppKey"] = "999998";
			client.DownloadProgressChanged += (sender, e) => {
				Console.WriteLine ("Bytes: " + e.BytesReceived + " of " + e.TotalBytesToReceive);
			};
			client.DownloadDataAsync (uri);
			client.DownloadDataCompleted += (s, e) => {

				if (e.Result != null) {

					var text = System.Text.Encoding.UTF8.GetString (e.Result);
					//var text = e.Result;
					var json = (JObject)JsonConvert.DeserializeObject (text);

					var array = (JArray)json ["Results"];
				
					var list = array.Select (item => new Booking ((int?)item ["canceled"], (int?)item ["attended"], (int)item ["studentID"], (int)item ["workshopID"], (string)item ["topic"], (string)item ["description"], (DateTime)item ["starting"], (DateTime)item ["ending"], (DateTime?)item ["BookingArchived"], (int)item ["campusID"])); 

					List<Booking> pendingBookings = new List<Booking> ();

					foreach (Booking booking in list.ToList()) {
						if (booking.BookingArchived == null) {
							pendingBookings.Add (booking);
						}
					}
					bookingSet = new BookingSet ();
					bookingSet.bookings = pendingBookings;
					fillWorkshopFragment (bookingSet);
					progress.Dismiss ();
					client.Dispose ();

				} else {
					WebClientOperations.WebClientError ("Couldn't find bookings", client, Application.Context);
					progress.Dismiss ();
				}

			};

		}

		public void fetchPastBookings ()
		{
			progress = getProgess ("Searching for past bookings...");

			Uri uri = new Uri (GetString (Resource.String.service_domain) + "workshop/booking/search?pageSize=100&studentId=" + studentIdPrefs);
			var client = new MyWebClient ();
			client.Headers ["AppKey"] = "999998";
			client.DownloadProgressChanged += (sender, e) => {
				Console.WriteLine ("Bytes: " + e.BytesReceived + " of " + e.TotalBytesToReceive);
			};
			client.DownloadDataAsync (uri);
			client.DownloadDataCompleted += (s, e) => {

				if (e.Result != null) {

					var text = System.Text.Encoding.UTF8.GetString (e.Result);
					//var text = e.Result;
					var json = (JObject)JsonConvert.DeserializeObject (text);

					var array = (JArray)json ["Results"];

					var list = array.Select (item => new Booking ((int?)item ["canceled"], (int?)item ["attended"], (int)item ["studentID"], (int)item ["workshopID"], (string)item ["topic"], (string)item ["description"], (DateTime)item ["starting"], (DateTime)item ["ending"], (DateTime?)item ["BookingArchived"], (int)item ["campusID"])); 

					List<Booking> pastBookings = new List<Booking> ();

					foreach (Booking booking in list.ToList()) {
						if (booking.BookingArchived != null && booking.Attended == 1)
							pastBookings.Add (booking);
					}

					bookingSet = new BookingSet ();
					bookingSet.bookings = pastBookings;

					fillPastFragment (bookingSet);

					progress.Dismiss ();
					client.Dispose ();

				} else {
					WebClientOperations.WebClientError ("Couldn't find bookings", client, Application.Context);
					progress.Dismiss ();
				}

			};

		}

		public void fetchSessionBookings ()
		{
			progress = getProgess ("Searching for session bookings...");

			Uri uri = new Uri (GetString (Resource.String.service_domain) + "session/booking/search?pageSize=50&studentId=" + studentIdPrefs);
			var client = new MyWebClient ();
			client.Headers ["AppKey"] = "999998";
			client.DownloadProgressChanged += (sender, e) => {
				Console.WriteLine ("Bytes: " + e.BytesReceived + " of " + e.TotalBytesToReceive);
			};
			client.DownloadDataAsync (uri);
			client.DownloadDataCompleted += (s, e) => {

				if (e.Result != null) {

					var text = System.Text.Encoding.UTF8.GetString (e.Result);
					//var text = e.Result;
					var json = (JObject)JsonConvert.DeserializeObject (text);
					Console.WriteLine (json);

					var array = (JArray)json ["Results"];

					var list = array.Select (xyz => new Session ((string)xyz ["LecturerFirstName"], (string)xyz ["LecturerLastName"], (string)xyz ["LecturerEmail"], (string)xyz ["SessionTypeAbb"], (string)xyz ["SessionType"], (string)xyz ["AssignmentType"], (string)xyz ["AppointmentType"], 
						           (int?)xyz ["BookingId"], 
						           (DateTime)xyz ["StartDate"], 
						           (DateTime)xyz ["EndDate"], 
						           (string)xyz ["Campus"], (bool?)xyz ["Cancel"], (string)xyz ["Assistance"], 
						           (string)xyz ["Reason"], (int?)xyz ["Attended"], 
						           (int?)xyz ["WaitingId"], (int?)xyz ["IsGroup"], (string)xyz ["NumPeople"],
						           (string)xyz ["LecturerComment"], (string)xyz ["LearningIssues"], (int?)xyz ["IsLocked"], (string)xyz ["AssignTypeOther"], (string)xyz ["Subject"], (string)xyz ["AppointmentsOther"], (string)xyz ["AssistanceText"], (int?)xyz ["SessionId"], (DateTime?)xyz ["archived"]));

					List<Session> sessionBookings = new List<Session> ();

					foreach (Session session in list.ToList()) {
						if (session.Cancel == false) {
							if (session.Attended != 1) {
								sessionBookings.Add (session);
							}
						}
					}

					sessionSet = new SessionSet ();
					sessionSet.sessions = sessionBookings;

					fillSessionFragment (sessionSet);

					progress.Dismiss ();
					client.Dispose ();

				} else {
					WebClientOperations.WebClientError ("Couldn't find bookings", client, Application.Context);
					progress.Dismiss ();
				}

			};

		}


		private ProgressDialog getProgess (String message)
		{
			progress = new ProgressDialog (this);
			progress.Indeterminate = true;
			progress.SetProgressStyle (ProgressDialogStyle.Spinner);
			progress.SetMessage (message);
			progress.SetCancelable (false);
			return progress;
		}


		private void setupNavDrawer ()
		{
			mDrawer = FindViewById<DrawerLayout> (Resource.Id.drawer_layout);
			mDrawerList = FindViewById<ListView> (Resource.Id.left_drawer);
			mDrawerList.Tag = 0;
			String[] navTitles = Resources.GetStringArray (Resource.Array.nav_array);

			mAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, navTitles);
			mDrawerList.Adapter = mAdapter;
			mDrawerList.ItemClick += (sender, args) => SelectItem (args.Position);

			mDrawerToggle = new MyActionBarDrawerToggle (
				this,
				mDrawer,
				0,
				0);

			mDrawer.SetDrawerListener (mDrawerToggle);
			SupportActionBar.SetHomeButtonEnabled (true);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);
			SupportActionBar.SetDisplayShowTitleEnabled (true);
			mDrawerToggle.SyncState ();
		}

		public List<Booking> getBookings ()
		{
			return bookingSet.bookings;
		}


		private void SelectItem (int position)
		{

			mDrawerList.SetItemChecked (position, true);

			switch (position) {

			//Home
			case 0:
				break;

			//Settings
			case 1: 
				StartActivity (typeof(SettingsActivity));
				break;

			//Make booking
			case 2:
				StartActivity (typeof(WorkshopChooser));
				break;
			//FAQ
			case 3:
				Toast.MakeText (this, "FAQ", ToastLength.Short).Show ();
				var uri = Android.Net.Uri.Parse ("https://helps-booking.uts.edu.au/index.cfm?scope=help");
				var intent = new Intent (Intent.ActionView, uri);
				StartActivity (intent);
				break;
			
			//Programs
			case 4:
				StartActivity (typeof(SessionChooserPrograms));
				break;

			}
			mDrawer.CloseDrawers ();
			mDrawerToggle.SyncState ();		
		}


		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {

			case Android.Resource.Id.Home:
				//The hamburger icon was clicked which means the drawer toggle will handle the event
				mDrawerToggle.OnOptionsItemSelected (item);
				return true;

			case Resource.Id.action_settings:
				//Refresh
				StartActivity (typeof(SettingsActivity));
				return true;

			case Resource.Id.action_bottom_bookings:
				//Refresh
				fetchWorkshopBookings ();				
				return true;
			
			case Resource.Id.action_bottom_past:
				//Refresh
				fetchPastBookings ();			
				return true;

			case Resource.Id.action_bottom_sessions:
				//Refresh
				fetchSessionBookings ();
				return true;

			
			default:
				return base.OnOptionsItemSelected (item);
			}
		}

		private void makeBottomFragVisible ()
		{
			FrameLayout bottomFrag = FindViewById<FrameLayout> (Resource.Id.fragment_bottom_container);
			bottomFrag.Visibility = ViewStates.Visible;
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.action_menu_account, menu);
			return base.OnCreateOptionsMenu (menu);
		}

		protected override void OnPostCreate (Bundle savedInstanceState)
		{
			base.OnPostCreate (savedInstanceState);
			mDrawerToggle.SyncState ();
		}



	}
}


