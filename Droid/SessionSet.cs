﻿using System;
using System.Collections.Generic;
using Java.Lang;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using Android.App;
using System.Text.RegularExpressions;
using Javax.Security.Auth;

namespace testDB.Droid
{
	public class Session
	{
		public string LecturerFirstName { get; private set; }

		public string LecturerLastName { get; private set; }

		public string LecturerEmail { get; private set; }

		public string SessionTypeAbb { get; private set; }

		public string SessionType { get; private set; }

		public string Time { get; private set; }


		public string AssignmentType { get; private set; }

		public string AppointmentType { get; private set; }

		public int? BookingId { get; private set; }

		public string StartDate { get; private set; }

		public string EndDate { get; private set; }

		public string Campus  { get; private set; }

		public bool? Cancel  { get; private set; }

		public string Assistance { get; private set; }

		public int? Attended{ get; private set; }

		public string Reason { get; private set; }

		public int? WaitingID { get; private set; }

		public int? IsGroup { get; private set; }

		public string NumPeople { get; private set; }

		public string LecturerComment { get; private set; }

		public string LearningIssues { get; private set; }

		public int? IsLocked { get; private set; }

		public string AssignTypeOther { get; private set; }

		public string SubjectTopic { get; private set; }

		public string Appointments { get; private set; }

		public string AppointmentsOther { get; private set; }

		public string AssistanceText { get; private set; }

		public int? SessionId { get; private set; }

		public DateTime? Archived { get; private set; }

		public Session (string lecturerFirstName, string lecturerLastName, string lecturerEmail, string sessionTypeAbb, string sessionType, string assignmentType, string appointmentType, 
		                int? bookingId, DateTime startDate, DateTime endDate, string campus, bool? cancel, string assistance, string reason, int? attended, int? waitingId, int? isGroup, string numPeople,
		                string lecturerComment, string learningIssues, int? isLocked, string assignTypeOther, string subjectTopic, string appointmentsOther, string assistanceText, int? sessionId, DateTime? archived)
		{
			LecturerFirstName = lecturerFirstName;

			LecturerLastName = lecturerLastName;

			LecturerEmail = lecturerEmail;

			SessionTypeAbb = sessionTypeAbb;

			SessionType = sessionType;

			Time = getTime (startDate, endDate);


			AssignmentType = assignmentType;

			AppointmentType = appointmentType;

			BookingId = bookingId;

			StartDate = getDate (startDate);

			EndDate = getDate (endDate);

			Campus = campus;

			Cancel = cancel;

			Assistance = assistance;

			Attended = attended;

			Reason = reason;

			WaitingID = waitingId;

			IsGroup = isGroup;

			NumPeople = numPeople;

			LecturerComment = lecturerComment;

			LearningIssues = learningIssues;

			IsLocked = isLocked;

			AssignTypeOther = assignTypeOther;

			SubjectTopic = subjectTopic;


			AppointmentsOther = appointmentsOther;

			AssistanceText = assistanceText;

			SessionId = sessionId;

			Archived = archived;

		}



		private string getTime (DateTime starting, DateTime ending)
		{
			string returnTime = starting.ToString ("HH:mm") + " - " + ending.ToString ("HH:mm");

			return returnTime;

		}

		private string getDate (DateTime dateTime)
		{
			string returnDate = dateTime.ToString ("dd/MM/yyyy HH:mm");
			return returnDate;
		}

	}

	public class SessionSet : Java.Lang.Object
	{

		public int Id{ get; private set; }

		public string AbbName{ get; private set; }

		public string FullName{ get; private set; }

		public bool IsCurrent{ get; private set; }


		public List<Session> sessions = new List<Session> ();

		public SessionSet (int id, string abbName, string fullName, bool isCurrent)
		{
			AbbName = abbName;

			FullName = fullName;

			IsCurrent = isCurrent;

			Id = id;

		}

		public SessionSet ()
		{

		}
	}
}

