﻿using System;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;



namespace testDB.Droid
{
	public class WorkshopAdapter : RecyclerView.Adapter
	{


		//item clicks
		public event EventHandler<int> ItemClick;

		//data set
		public WorkshopSet mWorkshopSet;

		// load adapter with data set at constructor
		public WorkshopAdapter (WorkshopSet workshopSet)
		{

			mWorkshopSet = workshopSet;

		}

		public override RecyclerView.ViewHolder
		OnCreateViewHolder (ViewGroup parent, int viewType)
		{
			View itemView = LayoutInflater.From (parent.Context).
				Inflate (Resource.Layout.SessionView, parent, false);
			WorkshopViewHolder vh = new WorkshopViewHolder (itemView, OnClick);
			return vh;
		}

		public override void
		OnBindViewHolder (RecyclerView.ViewHolder holder, int position)
		{
			WorkshopViewHolder vh = holder as WorkshopViewHolder;
			vh.workshopTopic.Text = mWorkshopSet.workshops [position].Topic;
			vh.workshopStart.Text = mWorkshopSet.workshops [position].StartDate;
			vh.workshopFinish.Text = mWorkshopSet.workshops [position].EndDate;
			vh.workshopTime.Text = mWorkshopSet.workshops [position].Time;
			vh.workshopLocation.Text = mWorkshopSet.workshops [position].Campus;
			vh.workshopPlaces.Text = mWorkshopSet.workshops [position].getPlaces ().ToString ();


		}

		public override int ItemCount {
			get { return mWorkshopSet.workshops.Count; }
		}

		void OnClick (int position)
		{
			if (ItemClick != null)
				ItemClick (this, position);
		}

	}


	public class WorkshopViewHolder : RecyclerView.ViewHolder
	{

		public TextView workshopTopic { get; private set; }

		public TextView workshopLocation { get; private set; }

		public TextView workshopPlaces { get; private set; }

		public TextView workshopStart { get; private set; }

		public TextView workshopFinish { get; private set; }

		public TextView workshopTime { get; private set; }



		public WorkshopViewHolder (View itemView, Action<int> listener) : base (itemView)
		{

			workshopTopic = itemView.FindViewById<TextView> (Resource.Id.tv_topic);
			workshopStart = itemView.FindViewById<TextView> (Resource.Id.tv_start);
			workshopFinish = itemView.FindViewById<TextView> (Resource.Id.tv_finish);
			workshopTime = itemView.FindViewById<TextView> (Resource.Id.tv_time);
			workshopLocation = itemView.FindViewById<TextView> (Resource.Id.tv_location);
			workshopPlaces = itemView.FindViewById<TextView> (Resource.Id.tv_places);

			itemView.Click += (sender, e) => listener (base.Position);


		}



			
	}
}

