﻿using System;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Opengl;



namespace testDB.Droid
{
	public class WorkshopPastAdapter : RecyclerView.Adapter
	{


		//item clicks
		public event EventHandler<int> ItemClick;

		//data set
		public BookingSet mBookingSet;

		// load adapter with data set at constructor
		public WorkshopPastAdapter (BookingSet bookingSet)
		{

			mBookingSet = bookingSet;

		}

		public override RecyclerView.ViewHolder
		OnCreateViewHolder (ViewGroup parent, int viewType)
		{
			View itemView = LayoutInflater.From (parent.Context).
				Inflate (Resource.Layout.WorkshopPastBookingView, parent, false);
			PastViewHolder vh = new PastViewHolder (itemView, OnClick);
			return vh;
		}

		public override void
		OnBindViewHolder (RecyclerView.ViewHolder holder, int position)
		{
			PastViewHolder vh = holder as PastViewHolder;
			DateTime endDate = DateTime.ParseExact (mBookingSet.bookings [position].EndDate, "dd/MM/yyyy HH:mm", null);

			if (endDate < DateTime.Now)
				vh.showImg ();

			vh.workshopTopic.Text = mBookingSet.bookings [position].Topic;
			vh.workshopStart.Text = mBookingSet.bookings [position].StartDate;
			vh.workshopFinish.Text = mBookingSet.bookings [position].EndDate;


		}

		public override int ItemCount {
			get { return mBookingSet.bookings.Count; }
		}

		void OnClick (int position)
		{
			if (ItemClick != null)
				ItemClick (this, position);
		}

	}


	public class PastViewHolder : RecyclerView.ViewHolder
	{


		public TextView workshopTopic { get; private set; }

		public TextView workshopStart { get; private set; }

		public TextView workshopFinish { get; private set; }

		private View ItemView;


		public PastViewHolder (View itemView, Action<int> listener) : base (itemView)
		{
			ItemView = itemView;
			workshopTopic = itemView.FindViewById<TextView> (Resource.Id.tv_topic);
			workshopStart = itemView.FindViewById<TextView> (Resource.Id.tv_start);
			workshopFinish = itemView.FindViewById<TextView> (Resource.Id.tv_finish);

			itemView.Click += (sender, e) => listener (base.Position);

		}

		public void showImg ()
		{
			var llView = ItemView.FindViewById<LinearLayout> (Resource.Id.ll_background);

			llView.SetBackgroundResource (Resource.Drawable.relative_border_accent);

		}
	}
}

