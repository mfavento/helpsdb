﻿using System;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;



namespace testDB.Droid
{
	public class SessionAdapter : RecyclerView.Adapter
	{


		//item clicks
		public event EventHandler<int> ItemClick;

		//data set
		public SessionSet mSessionSet;

		// load adapter with data set at constructor
		public SessionAdapter (SessionSet workshopSet)
		{

			mSessionSet = workshopSet;

		}

		public override RecyclerView.ViewHolder
		OnCreateViewHolder (ViewGroup parent, int viewType)
		{
			View itemView = LayoutInflater.From (parent.Context).
				Inflate (Resource.Layout.SessionProgramView, parent, false);
			SessionViewHolder vh = new SessionViewHolder (itemView, OnClick);
			return vh;
		}

		public override void
		OnBindViewHolder (RecyclerView.ViewHolder holder, int position)
		{
			SessionViewHolder vh = holder as SessionViewHolder;
			vh.sessionType.Text = mSessionSet.sessions [position].SessionType;
			vh.workshopStart.Text = mSessionSet.sessions [position].StartDate;
			vh.workshopFinish.Text = mSessionSet.sessions [position].EndDate;
			vh.workshopTime.Text = mSessionSet.sessions [position].Time;
			vh.sessionLecturer.Text = mSessionSet.sessions [position].LecturerFirstName + " " + mSessionSet.sessions [position].LecturerLastName;



		}

		public override int ItemCount {
			get { return mSessionSet.sessions.Count; }
		}

		void OnClick (int position)
		{
			if (ItemClick != null)
				ItemClick (this, position);
		}

	}


	public class SessionViewHolder : RecyclerView.ViewHolder
	{

		public TextView sessionType { get; private set; }


		public TextView sessionLecturer { get; private set; }

		public TextView workshopTopic { get; private set; }

		public TextView workshopStart { get; private set; }

		public TextView workshopFinish { get; private set; }

		public TextView workshopTime { get; private set; }



		public SessionViewHolder (View itemView, Action<int> listener) : base (itemView)
		{

			sessionType = itemView.FindViewById<TextView> (Resource.Id.tv_topic);
			workshopStart = itemView.FindViewById<TextView> (Resource.Id.tv_start);
			workshopFinish = itemView.FindViewById<TextView> (Resource.Id.tv_finish);
			workshopTime = itemView.FindViewById<TextView> (Resource.Id.tv_time);
			sessionLecturer = itemView.FindViewById<TextView> (Resource.Id.tv_lecturer);

			itemView.Click += (sender, e) => listener (base.Position);


		}



			
	}
}

