﻿using System;
using System.Collections.Generic;
using Java.Lang;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Globalization;

namespace testDB.Droid
{
	public class Workshop
	{
		public int WorkshopId{ get; private set; }

		public string Topic{ get; private set; }

		public string Description{ get; private set; }

		public string TargetingGroup { get; private set; }

		public string Campus { get; private set; }

		public string StartDate { get; private set; }

		public string EndDate { get; private set; }

		public int Maximum { get; private set; }

		public int WorkShopSetID { get; private set; }

		public string Cutoff { get; private set; }

		public string Type { get; private set; }

		public string Time { get; private set; }

		public int ReminderNum { get; private set; }

		public int ReminderSent { get; private set; }

		public int BookingCount { get; private set; }

		public DateTime? Archived { get; private set; }

		public Workshop (int workshopId, string topic, string description, string targetingGroup, string campus, DateTime startDate,
		                 DateTime endDate, int maximum, int workshopSetId, string cutoff, string type, int reminderNum, int reminderSent,
		                 int bookingCount, DateTime? archived)
		{
			WorkshopId = workshopId;
			Topic = topic;
			Description = description;
			TargetingGroup = targetingGroup;
			Campus = campus;
			Time = getTime (startDate, endDate);

			StartDate = getDate (startDate);
			EndDate = getDate (endDate);
			Maximum = maximum;
			WorkShopSetID = workshopSetId;
			Cutoff = cutoff;
			Type = type;
			ReminderNum = reminderNum;
			ReminderSent = reminderNum;
			BookingCount = bookingCount;
			Archived = archived;

		}

		public int getPlacesAmt ()
		{
			int result = Maximum - BookingCount;
			return result;
		}

		public bool getPlaces ()
		{
			int result = getPlacesAmt ();

			if (result > 0) {
				return true;

			} else {
				return false;
			}

		}

		private string getTime (DateTime starting, DateTime ending)
		{
			string returnTime = starting.ToString ("HH:mm") + " - " + ending.ToString ("HH:mm");

			return returnTime;

		}

		private string getDate (DateTime dateTime)
		{
			string returnDate = dateTime.ToString ("dd/MM/yyyy HH:mm");
			return returnDate;
		}

	}

	public class WorkshopSet : Java.Lang.Object
	{
		public string Name{ get; private set; }

		public int Id{ get; private set; }

		public List<Workshop> workshops = new List<Workshop> ();

		public WorkshopSet (int id, string name)
		{
			Name = name;
			Id = id;

		}
	}
}

