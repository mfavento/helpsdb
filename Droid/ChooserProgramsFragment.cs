﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Support.V7.Widget;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.Widget;
using Android.Preferences;


namespace testDB.Droid
{
	public class ChooserProgramsFragment : Fragment
	{

		private SwipeRefreshLayout mSwipeRefreshLayout;
		private View mView;
		private SessionSet mSessionSet;
		private int mSessionSetId;
		private RecyclerView mRecyclerView;
		private RecyclerView.LayoutManager mLayoutManager;
		private List<Session> sessions;
		private SessionAdapter mAdapter;
		private ProgressBar mProgress;
		private TextView txtNoneFound;
		private bool sessionsLoaded = false;


		ISharedPreferences prefs;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{

			mView = inflater.Inflate (Resource.Layout.ChooserFragment, container, false);

			mSwipeRefreshLayout = mView.FindViewById<SwipeRefreshLayout> (Resource.Id.swipeRefreshLayout);

			swipeRefreshSetup ();
			mProgress = mView.FindViewById<ProgressBar> (Resource.Id.pb_loading);
			if (mSessionSet.sessions.Count < 1)
				mProgress.Visibility = ViewStates.Visible;
			Button btnBack = mView.FindViewById<Button> (Resource.Id.btn_back);
			txtNoneFound = mView.FindViewById<TextView> (Resource.Id.txt_none_found);

			btnBack.Click += delegate {
				Activity.StartActivity (typeof(AccountOverview));

			};

			mRecyclerView = mView.FindViewById<RecyclerView> (Resource.Id.rcl_sessions);
			mLayoutManager = new LinearLayoutManager (this.Activity);
			mRecyclerView.SetLayoutManager (mLayoutManager);
			mAdapter = new SessionAdapter (mSessionSet);
			mAdapter.ItemClick += OnItemClick;
			mRecyclerView.SetAdapter (mAdapter);

			fetchWorkshops ();
			return mView;
					
		}

		private void swipeRefreshSetup ()
		{
			mSwipeRefreshLayout.Refresh += delegate {
				refreshAdapter ();
				mSwipeRefreshLayout.Refreshing = false;
			};
		}

		public void addListing (SessionSet sessionSet)
		{
			mSessionSet = sessionSet;
			mSessionSetId = sessionSet.Id;
		}


		/*
		 * This will pass data to the workshop fragment
		 */

		void OnItemClick (object sender, int position)
		{
			emailLecturer (position);


		}

		private void emailLecturer (int position)
		{
			prefs = PreferenceManager.GetDefaultSharedPreferences (Application.Context); 
			var studentIdPrefs = prefs.GetString ("StudentIdNumber", String.Empty);
			var lecturerEmail = sessions [position].LecturerEmail;
			var sessionTopicString = "Session Name: " + sessions [position].SessionType + " at " + sessions [position].StartDate;
			try {
				var email = new Intent (Android.Content.Intent.ActionSend);
				email.PutExtra (Android.Content.Intent.ExtraEmail, 
					new string[]{ lecturerEmail });

				email.PutExtra (Android.Content.Intent.ExtraSubject, sessionTopicString);
				email.PutExtra (Android.Content.Intent.ExtraText,
					"Type here why you would like to attend this session\n\n\nRegards,\nStudent Number: " + studentIdPrefs);
				email.SetType ("message/rfc822");

				StartActivity (email);
			} catch {
				Toast.MakeText (this.Activity, "Please install an email application on your phone to email lecturer from this medium", ToastLength.Short).Show ();

			}

		}


		/*
		 * Created to ensure refreshing of the adapter can be done anywhere
		 */

		public void refreshAdapter ()
		{
			fetchWorkshops ();
			mAdapter.NotifyDataSetChanged ();
		}
			
		/*
		 * This method returns a progress dialog with all the needed fields completed
		 */



		public void sortWorkshops (int sortChoice)
		{
			switch (sortChoice) {

			case Constants.SORT_TYPE:
				if (sessionsLoaded) {
					
					List<Session> TopicList = sessions.OrderBy (o => o.SessionType).ToList ();
					mSessionSet.sessions = TopicList;
				}
				break;
			
			case Constants.SORT_LOCATION:
				if (sessionsLoaded) {
					
					List<Session> LocationList = sessions.OrderBy (o => o.Campus).ToList ();
					mSessionSet.sessions = LocationList;
				}
				break;
			
			case Constants.SORT_DATE:
				if (sessionsLoaded) {
					
					List<Session> DateList = sessions.OrderBy (o => DateTime.ParseExact (o.StartDate, "dd/MM/yyyy HH:mm", null)).ToList ();
					mSessionSet.sessions = DateList;
				}
				break;
			
			case Constants.SORT_LECTURER:
				if (sessionsLoaded) {
					
					List<Session> LecturerList = sessions.OrderBy (o => o.LecturerFirstName).ToList ();
					mSessionSet.sessions = LecturerList;
				}
				break;
			}

			mAdapter.NotifyDataSetChanged ();


		}

		private void fetchWorkshops ()
		{
			Uri uri = new Uri ("http://helpsdb.cloudapp.net/api/session/booking/search?active=true&pageSize=50&SessionTypeId=" + mSessionSetId);
			var client = new MyWebClient ();

			client.Proxy = null;
			client.Headers ["AppKey"] = "999998";

			if (WebClientOperations.TestConnectionDB (client)) {
				client.DownloadDataAsync (uri);
			} else {
				WebClientOperations.WebClientError ("No result from server!", client, Application.Context);
				mProgress.Visibility = ViewStates.Gone;

			}

			client.DownloadDataCompleted += (s, e) => {

				if (e.Result != null) {
					
					var text = System.Text.Encoding.UTF8.GetString (e.Result);

					var json = (JObject)JsonConvert.DeserializeObject (text);

					var array = (JArray)json ["Results"];

					var list = array.Select (xyz => new Session ((string)xyz ["LecturerFirstName"], (string)xyz ["LecturerLastName"], (string)xyz ["LecturerEmail"], (string)xyz ["SessionTypeAbb"], (string)xyz ["SessionType"], (string)xyz ["AssignmentType"], (string)xyz ["AppointmentType"], 
						           (int?)xyz ["BookingId"], 
						           (DateTime)xyz ["StartDate"], 
						           (DateTime)xyz ["EndDate"], 
						           (string)xyz ["Campus"], (bool?)xyz ["Cancel"], (string)xyz ["Assistance"], 
						           (string)xyz ["Reason"], (int?)xyz ["Attended"], 
						           (int?)xyz ["WaitingId"], (int?)xyz ["IsGroup"], (string)xyz ["NumPeople"],
						           (string)xyz ["LecturerComment"], (string)xyz ["LearningIssues"], (int?)xyz ["IsLocked"], (string)xyz ["AssignTypeOther"], (string)xyz ["Subject"], (string)xyz ["AppointmentsOther"], (string)xyz ["AssistanceText"], (int?)xyz ["SessionId"], (DateTime?)xyz ["archived"]));

					sessions = list.ToList ();

					foreach (Session session in sessions.ToList()) {
						//this line will show all workshops
						if (session.Archived != null) {
							//this line will show only bookings in the future
							//if (workshop.Archived != null || DateTime.ParseExact (workshop.EndDate, "dd/MM/yyyy HH:mm", null) < DateTime.Now) {
							sessions.Remove (session);
						} else {
							Console.WriteLine ("found");
						}
					}

					if (sessions.Count < 1) {
						//no workshops found
						//fill text field to display to user
						txtNoneFound.Visibility = ViewStates.Visible;

						txtNoneFound.Text = "No workshops found";

						Console.WriteLine ("None found!");

					} else {
						foreach (Session wrk in sessions) {
							Console.WriteLine (wrk.SessionTypeAbb);
						}
					}

					mSessionSet.sessions = sessions;
					mProgress.Visibility = ViewStates.Gone;

					mAdapter.NotifyDataSetChanged ();
					sessionsLoaded = true;


				} else {
					WebClientOperations.WebClientError ("Couldn't find workshops", client, Application.Context);

				}
			};

		}

	}
}

