﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Support.V7.Widget;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.Widget;


namespace testDB.Droid
{
	public class ChooserFragment : Fragment
	{

		private SwipeRefreshLayout mSwipeRefreshLayout;
		private View mView;
		private TextView txtNoneFound;
		private WorkshopSet mWorkshopSet;
		private int mWorkshopSetId;
		private RecyclerView mRecyclerView;
		private RecyclerView.LayoutManager mLayoutManager;
		private List<Workshop> workshops;
		private WorkshopAdapter mAdapter;
		private ProgressBar mProgress;
		private WorkshopChooser workshopChooser;
		private bool workshopsLoaded = false;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{

			mView = inflater.Inflate (Resource.Layout.ChooserFragment, container, false);

			mSwipeRefreshLayout = mView.FindViewById<SwipeRefreshLayout> (Resource.Id.swipeRefreshLayout);

			swipeRefreshSetup ();
			mProgress = mView.FindViewById<ProgressBar> (Resource.Id.pb_loading);
			if (mWorkshopSet.workshops.Count < 1)
				mProgress.Visibility = ViewStates.Visible;
			
			txtNoneFound = mView.FindViewById<TextView> (Resource.Id.txt_none_found);

			Button btnBack = mView.FindViewById<Button> (Resource.Id.btn_back);
			workshopChooser = (WorkshopChooser)this.Activity;

			btnBack.Click += delegate {
				Activity.StartActivity (typeof(AccountOverview));

			};

			mRecyclerView = mView.FindViewById<RecyclerView> (Resource.Id.rcl_sessions);
			mLayoutManager = new LinearLayoutManager (this.Activity);
			mRecyclerView.SetLayoutManager (mLayoutManager);
			mAdapter = new WorkshopAdapter (mWorkshopSet);
			mAdapter.ItemClick += OnItemClick;
			mRecyclerView.SetAdapter (mAdapter);

			fetchWorkshops ();
			return mView;
					
		}

		private void swipeRefreshSetup ()
		{
			mSwipeRefreshLayout.Refresh += delegate {
				refreshAdapter ();
				mSwipeRefreshLayout.Refreshing = false;
			};
		}

		public void addListing (WorkshopSet workshopSet)
		{
			mWorkshopSet = workshopSet;
			mWorkshopSetId = workshopSet.Id;
		}


		/*
		 * This will pass data to the workshop fragment
		 */

		void OnItemClick (object sender, int position)
		{
			workshopChooser.viewWorkshopFragment (mWorkshopSet.workshops [position]);
					
		}


		/*
		 * Created to ensure refreshing of the adapter can be done anywhere
		 */

		public void refreshAdapter ()
		{
			fetchWorkshops ();
			mAdapter.NotifyDataSetChanged ();
		}
			
		/*
		 * This method returns a progress dialog with all the needed fields completed
		 */



		public void sortWorkshops (int sortChoice)
		{
			switch (sortChoice) {

			case Constants.SORT_TOPIC:
				if (workshopsLoaded) {
					List<Workshop> TopicList = workshops.OrderBy (o => o.Topic).ToList ();
					mWorkshopSet.workshops = TopicList;
				}
				break;
			
			case Constants.SORT_LOCATION:
				if (workshopsLoaded) {
					List<Workshop> LocationList = workshops.OrderBy (o => o.Campus).ToList ();
					mWorkshopSet.workshops = LocationList;
				}
				break;
			
			case Constants.SORT_DATE:
				if (workshopsLoaded) {
					
					List<Workshop> DateList = workshops.OrderBy (o => DateTime.ParseExact (o.StartDate, "dd/MM/yyyy HH:mm", null)).ToList ();
					mWorkshopSet.workshops = DateList;
				}
				break;

			}

			mAdapter.NotifyDataSetChanged ();


		}

		private void fetchWorkshops ()
		{
			Uri uri = new Uri (GetString (Resource.String.service_domain) + "workshop/search?active=true&pageSize=200&workshopSetId=" + mWorkshopSetId);
			var client = new MyWebClient ();

			client.Proxy = null;
			client.Headers ["AppKey"] = "999998";

			if (WebClientOperations.TestConnectionDB (client)) {
				client.DownloadDataAsync (uri);
			} else {
				WebClientOperations.WebClientError ("No result from server!", client, Application.Context);
				mProgress.Visibility = ViewStates.Gone;

			}

			client.DownloadDataCompleted += (s, e) => {

				if (e.Result != null) {
					
					var text = System.Text.Encoding.UTF8.GetString (e.Result);

					var json = (JObject)JsonConvert.DeserializeObject (text);

					var array = (JArray)json ["Results"];

					var list = array.Select (xyz => new Workshop ((int)xyz ["WorkshopId"], (string)xyz ["topic"], (string)xyz ["description"],
						           (string)xyz ["targetingGroup"], (string)xyz ["campus"], (DateTime)xyz ["StartDate"], (DateTime)xyz ["EndDate"],
						           (int)xyz ["maximum"], (int)xyz ["WorkShopSetID"], (string)xyz ["cutoff"], (string)xyz ["type"],
						           (int)xyz ["reminder_num"], (int)xyz ["reminder_sent"], (int)xyz ["BookingCount"], (DateTime?)xyz ["archived"]
					           ));

					workshops = list.ToList ();

					foreach (Workshop workshop in workshops.ToList()) {
						//this line will show all workshops
						if (workshop.Archived != null) {
							//this line will show only bookings in the future
							//if (workshop.Archived != null || DateTime.ParseExact (workshop.EndDate, "dd/MM/yyyy HH:mm", null) < DateTime.Now) {
							workshops.Remove (workshop);
						} else {
							Console.WriteLine ("found");
						}
					}

					if (workshops.Count < 1) {
						//no workshops found
						//fill text field to display to user
						txtNoneFound.Visibility = ViewStates.Visible;

						txtNoneFound.Text = "No workshops found";
						Console.WriteLine ("None found!");

					} else {
						foreach (Workshop wrk in workshops) {
							Console.WriteLine (wrk.Topic);
						}
					}

					mWorkshopSet.workshops = workshops;
					mProgress.Visibility = ViewStates.Gone;
					workshopChooser.setFragLoadedTrue ();
					mAdapter.NotifyDataSetChanged ();
					workshopsLoaded = true;

				} else {
					WebClientOperations.WebClientError ("Couldn't find workshops", client, Application.Context);

				}
			};

		}

	}
}

